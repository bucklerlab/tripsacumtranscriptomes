#!/usr/bin/sh

#docker1 exec d5dbcd2a6727 /bin/bash -c "cd /workdir/Tf_against_CML247; \$PASAHOME/scripts/Launch_PASA_pipeline.pl -c alignAssembly.config -C -I 20000 -R -g Zm_CML247_REFERENCE_PANZEA.1.1.fa -t Tfloridanum_non-maize_transcripts_fpkm_over_1.fasta --ALIGNERS gmap --CPU 37 >& Tf_against_CML247_log  "  &

#docker1 exec d5dbcd2a6727 /bin/bash -c "cd /workdir/Tf_against_W22; \$PASAHOME/scripts/Launch_PASA_pipeline.pl -c alignAssembly.config -C -I 20000 -R -g GCA_001644905.2_Zm-W22-REFERENCE-NRGENE-2.0_genomic.fna -t Tfloridanum_non-maize_transcripts_fpkm_over_1.fasta --ALIGNERS gmap --CPU 18 >& Tf_against_W22_log  "  &

#docker1 exec d5dbcd2a6727 /bin/bash -c "cd /workdir/Tf_against_B104; \$PASAHOME/scripts/Launch_PASA_pipeline.pl -c alignAssembly.config -C -I 20000 -R -g Zm-B104-DRAFT-ISU_USDA-0.2.fa -t Tfloridanum_non-maize_transcripts_fpkm_over_1.fasta --ALIGNERS gmap --CPU 18 >& Tf_against_B104_log  "

#docker1 exec d5dbcd2a6727 /bin/bash -c "cd /workdir/Tf_against_PH207; \$PASAHOME/scripts/Launch_PASA_pipeline.pl -c alignAssembly.config -C -I 20000 -R -g ZmaysPH207_443_v1.0.hardmasked.fa -t Tfloridanum_non-maize_transcripts_fpkm_over_1.fasta --ALIGNERS gmap --CPU 39 >& Tf_against_PH207_log  "  &

docker1 exec d5dbcd2a6727 /bin/bash -c "cd /workdir/Tf_against_EP1; \$PASAHOME/scripts/Launch_PASA_pipeline.pl -c alignAssembly.config -C -I 20000 -R -g GCA_001984235.2_Zm-EP1-REFERENCE-TUM-1.0_genomic.fna -t Tfloridanum_non-maize_transcripts_fpkm_over_1.fasta --ALIGNERS gmap --CPU 39 >& Tf_against_EP1_log  "  

docker1 exec d5dbcd2a6727 /bin/bash -c "cd /workdir/Tf_against_F7; \$PASAHOME/scripts/Launch_PASA_pipeline.pl -c alignAssembly.config -C -I 20000 -R -g GCA_001990705.1_Zm-F7-REFERENCE-TUM-1.0_genomic.fna -t Tfloridanum_non-maize_transcripts_fpkm_over_1.fasta --ALIGNERS gmap --CPU 39 >& Tf_against_F7_log  "  

docker1 exec d5dbcd2a6727 /bin/bash -c "cd /workdir/Tf_against_Mo17; \$PASAHOME/scripts/Launch_PASA_pipeline.pl -c alignAssembly.config -C -I 20000 -R -g Mo17.fasta -t Tfloridanum_non-maize_transcripts_fpkm_over_1.fasta --ALIGNERS gmap --CPU 39 >& Tf_against_Mo17_log  "
