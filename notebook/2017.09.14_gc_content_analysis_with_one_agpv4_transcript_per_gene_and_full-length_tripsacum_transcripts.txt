## Christy Gault ##
## 09/14/17 ##
## GC content analysis with one agpv4 transcript per gene and full-length tripsacum transcripts ##

Here is the complete lab notebook entry for GC content along absolute transcript length.

I was looking at GC content of fully-assembled Tripsacum transcripts,
which were defined by aligning to at least 95% of a B73 AGPv3 protein's length.
I also looked at GC content for all transcripts of the closest AGPv4 maize homolog.
I realized that this wasn't a fair comparison to compare Tripsacum transcripts
with probably no introns to all the transcripts of the maize homolog (many of which
contain introns. I really want to pick the transcript of the maize homolog that aligns
to the most AGPv3 protein sequence. I should do this by aligning all the AGPv4 maize transcripts
to AGPv3 maize protein.

blastX run to align agpv4 transcripts against agpv3 protein:

/programs/ncbi-blast-2.3.0+/bin/makeblastdb -in Zea_mays.AGPv3.31.pep.all.fa -dbtype prot

/programs/ncbi-blast-2.3.0+/bin/blastx -db Zea_mays.AGPv3.31.pep.all.fa -query Zea_mays.AGPv4.cdna.all.fa -out Zea_mays.AGPv4.cdna.all_against_AGPv3_protein.outfmt6 -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qlen slen"  -evalue 1e-20 -num_threads 40 -max_target_seqs 1

Next I asked which agpv4 transcript was the best per agpv4 gene. It needed to:
1) align to the same agpv3 protein as it's tripsacum homolog (about 1200/8000 didn't, suprisingly)
2) have the longest alignment length compared to other agpv4 transcripts from the agpv4 gene

get_agpv4_transcript_with_longest_alignment_to_agpv3_protein.pl

Input:

Trinity_Tdactyloides_with_roots_no_adapters_only_streptophyta_against_B73_protein.outfmt6
maize_homologs_paired_with_full-length_Tdactyloides_transcripts.txt

Output:
full-length_Tdactyloides_transcript_agpv4_gene_and_transcript_homolog_best_agpv3_protein_hit.txt

The output file looks like this:

fully_assembled_tripsacum_transcript    agpv4_gene_homolog      agpv4_best_transcript   agpv3_protein_blast_hit
TRINITY_DN230908_c10_g1_i5      Zm00001d016733  Zm00001d016733_T001     GRMZM2G017991_P01
TRINITY_DN229297_c5_g1_i2       Zm00001d009005  Zm00001d009005_T002     GRMZM2G107205_P01
TRINITY_DN177217_c0_g1_i1       Zm00001d024292  Zm00001d024292_T002     GRMZM2G151580_P01
TRINITY_DN192703_c0_g3_i1       Zm00001d006872  Zm00001d006872_T001     GRMZM2G159218_P01

I need to make sure that the agpv3 protein is the same gene as
the agpv4 gene according to Doreen Ware's genome assembly.

Script:

get_stringent_homolog_pairs_for_gc_content.pl

Output:
stringent_Tdactyloides_maize_homolog_pairs_gc_content.txt
non-stringent_Tdactyloides_maize_homolog_pairs_gc_content.txt

These scripts got GC content along normalized and absolute transcript length for transcripts
between 500bp and 10,000 bp in length:

calculate_gc_along_transcript_length_B73_agpv4.pl
calculate_gc_along_transcript_length_B73_agpv3.pl
calculate_gc_along_transcript_length_Tdactyloides.pl
calculate_gc_along_transcript_length_Tfloridanum.pl

A while ago, I needed to get only the fully assembled Tripsacum transcripts and 
calculate GC by subgenome:

pull_out_gc_along_absolute_length_for_full_length_transcripts.pl

But now I need to get only the gc content for the stringent homolog pairs:

pull_out_gc_along_absolute_length_for_stringent_homolog_pairs.pl

Output:
gc_content_by_absolute_transcript_length_Tfloridanum_of_stringent_agpv4_homolog_pairs.txt
gc_content_by_absolute_transcript_length_agpv4_of_stringent_Tfloridanum_homolog_pairs.txt
gc_content_by_absolute_transcript_length_agpv4_of_stringent_Tdactyloides_homolog_pairs.txt
gc_content_by_absolute_transcript_length_Tdactyloides_of_stringent_agpv4_homolog_pairs.txt

Two scripts to separate full-length tripsacum and best agpv4 transcript by proteome.
Genes in the proteome are based on Steve Briggs study:

get_gc_content_along_transcript_length_for_tripsacum_proteome_genes.pl

Output:

gc_content_by_absolute_transcript_length_stringent_Tdactyloides_homologs_proteome.txt
gc_content_by_absolute_transcript_length_stringent_Tdactyloides_homologs_non_proteome.txt
gc_content_by_absolute_transcript_length_stringent_Tfloridanum_homologs_proteome.txt
gc_content_by_absolute_transcript_length_stringent_Tfloridanum_homologs_non_proteome.txt

get_gc_content_along_transcript_length_for_maize_proteome_genes.pl

Output:
gc_content_by_absolute_transcript_length_agpv4_stringent_homologs_of_full-length_Tdactyloides_transcripts_proteome.txt
gc_content_by_absolute_transcript_length_agpv4_stringent_homologs_of_full-length_Tdactyloides_transcripts_non_proteome.txt
gc_content_by_absolute_transcript_length_agpv4_stringent_homologs_of_full-length_Tfloridanum_transcripts_proteome.txt
gc_content_by_absolute_transcript_length_agpv4_stringent_homologs_of_full-length_Tfloridanum_transcripts_non_proteome.txt

R script to make graphs:

gc_content_along_absolute_transcript_length_full-length_transcripts.R