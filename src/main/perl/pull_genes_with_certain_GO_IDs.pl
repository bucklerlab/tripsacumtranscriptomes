#!/usr/bin/perl -w

use strict;

# This script is to pull out AGPv4 gene names with desired GO IDs
# usage: perl pull_genes_with_certain_GO_IDs.pl /Users/cg449/Desktop/tripsacumTranscriptomes/data/core_housekeeping_GOIDs.txt /Users/cg449/Desktop/tripsacumTranscriptomesData/maize.B73.AGPv4.aggregate.gaf B73_AGPv4_genes_with_conserved_GOIDs.txt

my $file1 = $ARGV[0]; # file with conserved GO IDs
my $file2 = $ARGV[1]; # file linking all maize genes to GO IDs
my $file3 = $ARGV[2]; # output file

my %desired_goids = ();

# Collect all GO IDs of interest into the %desired_goids hash

open(FH1, "<$file1") || die "Can't open FH1.\n";

while(<FH1>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\s+/, $line);
	
	foreach my $column (@columns) {
		if ($column =~ /^GO:\d+$/) {
			$desired_goids{$column} = 1;
		}
	}
}

close FH1;

# Scan through all genes and pull out the genes with GO IDs of interest

open(FH2, "<$file2") || die "Can't open FH2.\n";

open(OUT, ">$file3") || die "Can't open OUT.\n";

while(<FH2>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $gene_name = $columns[1];
	my $go = $columns[4];
	
	if (defined $desired_goids{$go}) {
		print OUT "$gene_name\n";
	}
}

close FH2;
close OUT;