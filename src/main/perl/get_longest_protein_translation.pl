#!/usr/bin/perl -w

use strict;

# This script is to get the likely protein encoded by the Tripsacum transcripts that aren't shared with maize have an FPKM > 1

# Usage: perl get_longest_protein_translation.pl Tdactyloides_non-maize_transcripts_fpkm_over_1.blastx.nr.outfmt6 Tdactyloides_non-maize_transcripts_fpkm_over_1_with_nr_hits_transeq.out Tdactyloides_non-maize_transcripts_fpkm_over_1_with_nr_hits_translations.fasta

use Bio::SeqIO;

my $file1 = $ARGV[0]; # Tdactyloides_non-maize_transcripts_fpkm_over_1.blastx.nr.outfmt6
my $file2 = $ARGV[1]; # Tdactyloides_non-maize_transcripts_fpkm_over_1_with_nr_hits_transeq.out
my $file3 = $ARGV[2]; # Tdactyloides_non-maize_transcripts_fpkm_over_1_with_nr_hits_translations.fasta

my %frames = (); # The coding frame of the transcript that had a blast hit

open FH1, "<$file1" || die "Can't open FH1.\n";

while(<FH1>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $transcript = $columns[0];
	my $evalue = $columns[6];
	my $bitscore = $columns[7];
	my $blast_frame = $columns[8];
	my $evalue_bitscore_frame = "$evalue".":"."$bitscore".":"."$blast_frame";

	# Multiple alignments exist between the query and the best target. The one with the lowest evalue and highest bitscore should be chosen to determine the translation frame
	if (exists $frames{$transcript}) {
		my $stored_ebf = $frames{$transcript};
		if ($stored_ebf =~ /([^:]+):([^:]+):([^:]+)/) {	
			my $stored_evalue = $1;
			my $stored_bitscore = $2;
			my $stored_frame = $3;
			if ($evalue < $stored_evalue) {
				if ($bitscore > $stored_bitscore) {
					if ($blast_frame > 0) { # reverse frames aren't possible because these are strand-specific data
						$frames{$transcript} = $evalue_bitscore_frame;
					}
				}
			}
		}
	}
	else {
		if ($blast_frame > 0) {
			$frames{$transcript} = $evalue_bitscore_frame;
		}
	}
}

close FH1;

my $seqio = Bio::SeqIO->new(-file=> "$file2", -format=> 'Fasta');
open OUT, ">$file3";

while(my $entry=$seqio->next_seq) {
	my $display_id = $entry->display_id;	
	if ($display_id =~ /(TRINITY\S+)_(\d)$/) {
		my $transcript = $1;
		my $transeq_frame = $2;
		if (exists $frames{$transcript}) {
			my $evalue_bitscore_frame = $frames{$transcript};
			if ($evalue_bitscore_frame =~ /[^:]+:[^:]+:([^:]+)/) {
				my $blast_frame = $1;

				# If the transeq translation is in the same frame as the best blast match, then get the longest open reading frame in that frame
				if ($transeq_frame == $blast_frame) {
					my $sequence = $entry->seq;
					my @sequences = split(/\*/,$sequence);
					my @sorted = sort { length $a <=> length $b } @sequences;
					my $longest_translation = pop @sorted;	
					print OUT &to_fasta($transcript,$longest_translation);
				}
			}
		}
	}
}

close OUT;

sub to_fasta {
        my ($seqName, $seq) = @_;
	my $len = 60;
	my $formatted_seq = ">$seqName\n";

	while (my $chunk = substr($seq, 0, $len, "")) {
		$formatted_seq .= "$chunk\n";
	}
	
	return $formatted_seq;
}


