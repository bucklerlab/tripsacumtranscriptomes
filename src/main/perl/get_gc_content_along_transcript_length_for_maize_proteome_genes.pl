#!/usr/bin/perl -w
use strict;

# This script is to parse the existing file containing GC content along absolute transcript length for full-length transcripts into two more files: proteome and non-proteome

# usage: perl get_gc_content_along_transcript_length_for_maize_proteome_genes.pl /Users/cg449/Desktop/tripsacummapping/data/gc_content/gc_content_by_absolute_transcript_length_agpv4_of_stringent_Tdactyloides_homolog_pairs.txt /Users/cg449/Desktop/tripsacummapping/data/gc_content/gc_content_by_absolute_transcript_length_agpv4_stringent_homologs_of_full-length_Tdactyloides_transcripts_proteome.txt /Users/cg449/Desktop/tripsacummapping/data/gc_content/gc_content_by_absolute_transcript_length_agpv4_stringent_homologs_of_full-length_Tdactyloides_transcripts_non_proteome.txt 

# usage: perl get_gc_content_along_transcript_length_for_maize_proteome_genes.pl /Users/cg449/Desktop/tripsacummapping/data/gc_content/gc_content_by_absolute_transcript_length_agpv4_of_stringent_Tfloridanum_homolog_pairs.txt /Users/cg449/Desktop/tripsacummapping/data/gc_content/gc_content_by_absolute_transcript_length_agpv4_stringent_homologs_of_full-length_Tfloridanum_transcripts_proteome.txt /Users/cg449/Desktop/tripsacummapping/data/gc_content/gc_content_by_absolute_transcript_length_agpv4_stringent_homologs_of_full-length_Tfloridanum_transcripts_non_proteome.txt

# Read in proteome genes from AGPv4:

open(REF, "</Users/cg449/Desktop/tripsacummapping/data/gc_content/proteome_geneID_AGPv4.txt") || die "Can't open REF.\n";

my %proteome = ();

while(<REF>) {
	my $line = $_;
	chomp($line);
	$proteome{$line} = 1;
}

close REF;

# parse GC content file into proteome and non-proteome:

my $file = $ARGV[0];
my $proteome_out = $ARGV[1];
my $non_proteome_out = $ARGV[2];

open(FH1, "<$file") || die "Can't open FH1.\n";
open(PROT, ">$proteome_out") || die "Can't open PROT.\n";
open(NON, ">$non_proteome_out") || die "Can't open NON.\n";

while(<FH1>) {
	my $line = $_;
	chomp($line);
	if ($line =~ /^transcript/) {
		print PROT "$line\n";
		print NON "$line\n";
	}

	else {
		my @columns = split(/\s+/, $line);
		my $current_transcript = $columns[0];
		my $current_gene = "";

		if ($current_transcript =~ /^([^_]+)_[^_]+/) { # Capture gene name before the underscore
			$current_gene = $1;
		}

		if (defined $proteome{$current_gene}) { # if the gene is in the proteome, print this line to the proteome file
			print PROT "$line\n";
		}

		else { # if the gene is in not the proteome, print this line to the non-proteome file
			print NON "$line\n"; 
		}
	}
}

close FH1;
close PROT;
close NON;


