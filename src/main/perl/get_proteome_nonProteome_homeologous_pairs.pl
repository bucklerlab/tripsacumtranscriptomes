#!/usr/bin/perl -w

use strict;

my $file1 = "/Users/cg449/Desktop/B73/AGP_v4/proteome_geneID_AGPv4.txt";

open(FH1, "<$file1") || die "Can't open FH1.\n";

my %proteome = ();

while(<FH1>) {
	my $line = $_;
	chomp($line);
	
	if ($line =~ /proteome_genes/) {
		# header line
	}
	
	else {
		$proteome{$line} = 1;
	}
}

close FH1;

my $file2 = "/Users/cg449/Desktop/tripsacummapping/data/transcriptome_assembly/maize1_maize2_homeologous_pairs_agpv4_names.txt";
my $outfile1 = "/Users/cg449/Desktop/tripsacummapping/data/transcriptome_assembly/maize1_maize2_homeologous_pairs_by_proteome.txt";

open(FH2, "<$file2") || die "Can't open FH2.\n";

open(OUT, ">$outfile1") || die "Can't open OUT.\n";

print OUT "maize1	maize2	ProteomeNonproteome\n";

while(<FH2>) {
	my $line = $_;
	chomp($line);
	
	if ($line =~ /^(Z\S+)\t(Z\S+)$/) {
		my $maize1 = $1;
		my $maize2 = $2;
		
		if (defined $proteome{$maize1}) {
			
			if (defined $proteome{$maize2}) {
				if ($maize1 ne $maize2) {				
					print OUT "$maize1	$maize2	1p2p\n";
				}
			}
			
			else {
				if ($maize1 ne $maize2) {				
					print OUT "$maize1	$maize2	1p2n\n";
				}
			}
		}
		
		else {
		
			if (defined $proteome{$maize2}) {
				if ($maize1 ne $maize2) {				
					print OUT "$maize1	$maize2	1n2p\n";
				}
			}
			
			else {
				if ($maize1 ne $maize2) {				
					print OUT "$maize1	$maize2	1n2n\n";
				}
			}
		}	
	}
}

close FH2;
close OUT;