#!/usr/bin/perl -w

use strict;

# This script is for parsing blast output (format 6) for your queries of interest
# usage: perl parse_blobology_input_by_query_name.pl > /Users/cg449/Desktop/tripsacummapping/data/transcriptome_assembly/Tripsacum_transcripts_absent_from_B73/Tdactyloides_non-B73_transcripts_fpkm_over_1_blobplot.txt
# usage: perl parse_blobology_input_by_query_name.pl > /Users/cg449/Desktop/tripsacummapping/data/transcriptome_assembly/Tripsacum_transcripts_absent_from_B73/Tfloridanum_non-B73_transcripts_fpkm_over_1_blobplot.txt


my $file1 = "/Users/cg449/Desktop/tripsacummapping/data/transcriptome_assembly/Tripsacum_transcripts_absent_from_B73/Tdactyloides_non-B73_transcripts_fpkm_over_1.txt";
my $file2 = "/Users/cg449/Desktop/tripsacummapping/data/transcriptome_assembly/Tripsacum_transcripts_absent_from_B73/Tdactyloides1_unaligned_pasa_transcripts_blobplot_FPKM.txt";

my %items_of_interest = ();
my %blast_queries_with_a_hit = ();

open(FH1, "<$file1") || die "Can't open FH1.\n"; # list of blast queries of interest

while(<FH1>) {
	my $query = $_;
	chomp($query);
	$items_of_interest{$query} = 1;
}

close FH1;

open(FH2, "<$file2") || die "Can't open FH2.\n"; # BLOBOLOGY file

while(<FH2>) {
	my $line = $_;
	chomp($line);
	my @cols = split(/\t/, $line);
	my $query = $cols[0];
	$blast_queries_with_a_hit{$query} = $line;
}

close FH2;

# I want transcripts that ARE in the blast output file:

foreach my $query (keys %items_of_interest) {
	if (defined $blast_queries_with_a_hit{$query}) {
		print "$blast_queries_with_a_hit{$query}\n";
	}
	
	else {
		# do nothing
	}
}