#!/usr/bin/perl -w

use strict;

# This script is for parsing blast output (format 6) for your queries of interest
# usage: perl parse_blast_hits_by_query_name.pl > /Users/cg449/Desktop/tripsacummapping/data/transcriptome_assembly/Tripsacum_transcripts_absent_from_B73/Tdactyloides_non-B73_transcripts_fpkm_over_1.txt
# usage: perl parse_blast_hits_by_query_name.pl > /Users/cg449/Desktop/tripsacummapping/data/transcriptome_assembly/Tripsacum_transcripts_absent_from_B73/Tfloridanum_non-B73_transcripts_fpkm_over_1.txt


my $file1 = "/Users/cg449/Desktop/tripsacummapping/data/transcriptome_assembly/Tripsacum_transcripts_absent_from_B73/Tfloridanum_all_tissues_unaligned_pasa_transcripts_blobplot_FPKM_greater_than_1.txt";
my $file2 = "/Users/cg449/Desktop/tripsacummapping/data/transcriptome_assembly/Tripsacum_transcripts_absent_from_B73/Tfloridanum_only_streptophyta_against_B73_cdna_dc-megablast.outfmt6";

my %items_of_interest = ();
my %blast_queries_with_a_hit = ();

open(FH1, "<$file1") || die "Can't open FH1.\n"; # list of blast queries of interest

while(<FH1>) {
	my $query = $_;
	chomp($query);
	$items_of_interest{$query} = 1;
}

close FH1;

open(FH2, "<$file2") || die "Can't open FH2.\n"; # blast output file

while(<FH2>) {
	my $line = $_;
	chomp($line);
	my @cols = split(/\t/, $line);
	my $query = $cols[0];
	$blast_queries_with_a_hit{$query} = 1;
}

close FH2;

# I want transcripts that were NOT in the blast output file:

foreach my $query (keys %items_of_interest) {
	if (defined $blast_queries_with_a_hit{$query}) {
		# do nothing
	}
	
	else {
		print "$query\n";
	}
}