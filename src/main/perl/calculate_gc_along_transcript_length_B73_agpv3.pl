#!/usr/bin/perl -w

use strict;
use Bio::SeqIO;
use Bio::Perl;

my $file2 = "B73_AGP_v3_cdna.fasta";
my $file3 = "gc_content_by_normalized_transcript_length_B73_agpv3.txt";
my $file4 = "gc_content_by_absolute_transcript_length_B73_agpv3.txt";

#my $file2 = "Trinity_Tdactyloides_with_roots_no_adapters_only_streptophyta.fasta";
#my $file3 = "gc_content_by_normalized_transcript_length_Tdactyloides.txt";
#my $file4 = "gc_content_by_absolute_transcript_length_Tdactyloides.txt";

# my $file2 = "Trinity_Tfloridanum_with_roots_no_adapters_only_streptophyta.fasta";
#my $file3 = "gc_content_by_normalized_transcript_length_Tfloridanum.txt";
#my $file4 = "gc_content_by_absolute_transcript_length_Tfloridanum.txt";

open(NORM, ">$file3") || die "Can't open FH3.\n";
my @normalized_positions = (1..100);
print NORM "transcript";

foreach my $column_header (@normalized_positions) {
	print NORM "	$column_header";
}

print NORM "\n";

open(ABS, ">$file4") || die "Can't open FH4.\n";
my @absolute_positions = (1..10000);
print ABS "transcript";

foreach my $col_header (@absolute_positions) {
	print ABS "	$col_header";
}

print ABS "\n";

my $seqio_object = Bio::SeqIO->new(-file => $file2, -format => 'fasta'); # Open cDNA file

while(my $current_object = $seqio_object->next_seq) {
	my $transcript_id = $current_object->id;
	my $sequence = $current_object->seq();
	my @bases = split(//, $sequence);
	my $i = 0; # Represents bp position in transcript
	my $transcript_length = @bases;
	my $missing_values = 10001 - $transcript_length;
	my @numerator = (); # number of GC bases at percentile
	my @denominator = (); # number of total bases at percentile
	my %normalized = ();

	if (($transcript_length < 10001) && ($transcript_length > 499)) { # Ignore transcripts longer than 10,000 bp. They probably aren't real anyway. # 500 bp is the cutoff for Trinity transcripts.
		
		print "$transcript_id	$transcript_length	$missing_values\n";
		print ABS "$transcript_id";
		foreach my $base (@bases) {
			$i++;
			my $abs_position = $i;
			my $norm_position = $i / $transcript_length;
			my $norm_position_rounded = sprintf("%.4f", $norm_position);

			if ( ($base eq 'G') || ($base eq 'C') ) {
				print ABS " 1";
				$normalized{$norm_position_rounded} = 1;
			}

			else {
				print ABS " 0";
				$normalized{$norm_position_rounded} = 0;
			}

		}

		for (my $z = 1; $z < 10001; $z++) {
			if ($z <= $missing_values) {
				print ABS " NA";
			}
		}


		print ABS "\n";

		my $j = 0;
		my $k = 1;
		
		for (1..100) {

			my $m = $k/100;
			my $n = $j/100;

			foreach my $norm_position_rounded (sort keys %normalized) {
				if ( ($norm_position_rounded > $n) && ($norm_position_rounded <= $m) ) {
					$numerator[$k] += $normalized{$norm_position_rounded};
					$denominator[$k]++;
				}
					
			}

			$j++;
			$k++;
		}

		print NORM "$transcript_id";
		my $l = 1; # place keeper to represent array element

		for (1..100) {
			my $gc_content = $numerator[$l] / $denominator[$l];
			my $gc_content_rounded = sprintf("%.4f", $gc_content);
			print NORM "	$gc_content_rounded";
			$l++;
		}
	
		print NORM "\n";
	}
}

close NORM;
close ABS;
