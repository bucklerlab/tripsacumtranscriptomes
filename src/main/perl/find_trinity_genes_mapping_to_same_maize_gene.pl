#!/usr/bin/perl -w

use strict;

# How many Trinity "genes" align to the same maize gene?
# This gives a measure of how Trinity may have overestimated gene number if it misidentified allelic transcripts as transcripts originating from different genes

my $file1 = "/Users/cg449/Desktop/Tripsacum/transcriptome_assembly/PASA/pasa_Tdactyloides/unique_agpv4_genes_aligned_to_Tdactyloides_transcripts_fuzzy_boundaries.txt";

open(FH1, "<$file1") || die "Can't open FH1.\n";

my %tdactyloides = (); #key1: maize_gene, key2: trinity gene

while(<FH1>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $maize_gene = shift (@columns);
	
	foreach my $trip_transcript (@columns) {
		if ($trip_transcript =~ /^(TRINITY.+)_i\d+$/) {
			my $trinity_gene = $1;
			$tdactyloides{$maize_gene}{$trinity_gene} = 1;		
		}
	}
}

foreach my $maize_gene (keys %tdactyloides) {
	my $num_trinity_genes = keys %{$tdactyloides{$maize_gene}};

	# Uncomment the line below to report how many trinity tdactyloides genes map to each maize gene
	#print "$maize_gene	$num_trinity_genes\n";
}

close FH1;


# Do the same with Tripsacum floridanum

my $file2 = "/Users/cg449/Desktop/Tripsacum/transcriptome_assembly/PASA/pasa_Tfloridanum/unique_agpv4_genes_aligned_to_Tfloridanum_transcripts_fuzzy_boundaries.txt";

open(FH2, "<$file2") || die "Can't open FH2.\n";

my %tfloridanum = (); #key1: maize_gene, key2: trinity gene

while(<FH2>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $maize_gene = shift (@columns);
	
	foreach my $trip_transcript (@columns) {
		if ($trip_transcript =~ /^(TRINITY.+)_i\d+$/) {
			my $trinity_gene = $1;
			$tfloridanum{$maize_gene}{$trinity_gene} = 1;		
		}
	}
}

foreach my $maize_gene (keys %tfloridanum) {
	my $num_trinity_genes = keys %{$tfloridanum{$maize_gene}};

	# Uncomment the line below to report how many trinity tdactyloides genes map to each maize gene
	print "$maize_gene	$num_trinity_genes\n";
}



close FH2;