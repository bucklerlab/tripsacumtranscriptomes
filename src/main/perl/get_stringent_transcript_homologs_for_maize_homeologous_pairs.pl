#!/usr/bin/perl -w

use strict;

#my $file1 = "/Users/cg449/Desktop/tripsacummapping/data/gc_content/gc_first_500bp_avg_by_maize_gene_homolog_td.txt";
#my $file2 = "/Users/cg449/Desktop/tripsacummapping/data/transcriptome_assembly/maize1_maize2_homeologous_pairs_by_proteome.txt";
#my $outfile1 = "/Users/cg449/Desktop/tripsacummapping/data/gc_content/maize1_maize2_homeologs_td_gc.txt";

my $file1 = "/Users/cg449/Desktop/tripsacummapping/data/gc_content/gc_first_500bp_avg_by_maize_gene_homolog_tf.txt";
my $file2 = "/Users/cg449/Desktop/tripsacummapping/data/transcriptome_assembly/maize1_maize2_homeologous_pairs_by_proteome.txt";
my $outfile1 = "/Users/cg449/Desktop/tripsacummapping/data/gc_content/maize1_maize2_homeologs_tf_gc.txt";


open(FH1, "<$file1") || die "Can't open FH1.\n";

my %avg_gcs = (); # key1: maize gene, key2: trip gc, value: maize gc

while(<FH1>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $maize_gene = $columns[0];
	my $trip_gc = $columns[1];
	my $maize_gc = $columns[2];
	$avg_gcs{$maize_gene} = "$trip_gc"."\t"."$maize_gc";
}

close FH1;

open(FH2, "<$file2") || die "Can't open FH2.\n";
open(OUT, ">$outfile1") || die "Can't open OUT.\n";
print OUT "maize1_homeolog	maize2_homeolog	proteome_nonproteome	maize1_b73_gc	maize1_trip_gc	maize2_b73_gc	maize2_trip_gc\n";

my %homeo_pairs = ();

while(<FH2>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $maize1 = $columns[0];
	my $maize2 = $columns[1];
	my $category = $columns[2];

	if (defined $avg_gcs{$maize1}) {
		if (defined $avg_gcs{$maize2}) {
			print OUT "$line	$avg_gcs{$maize1}	$avg_gcs{$maize2}\n";
		}
	}
}

close FH2;
close OUT;