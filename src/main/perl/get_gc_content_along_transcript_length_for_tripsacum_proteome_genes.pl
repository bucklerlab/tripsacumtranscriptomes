#!/usr/bin/perl -w
use strict;

# This script is to parse the existing file containing GC content along absolute transcript length for full-length transcripts into two more files: proteome and non-proteome

# perl get_gc_content_along_transcript_length_for_tripsacum_proteome_genes.pl /Users/cg449/Desktop/tripsacummapping/data/gc_content/gc_content_by_absolute_transcript_length_Tdactyloides_of_stringent_agpv4_homolog_pairs.txt /Users/cg449/Desktop/tripsacummapping/data/gc_content/gc_content_by_absolute_transcript_length_stringent_Tdactyloides_homologs_proteome.txt /Users/cg449/Desktop/tripsacummapping/data/gc_content/gc_content_by_absolute_transcript_length_stringent_Tdactyloides_homologs_non_proteome.txt /Users/cg449/Desktop/tripsacummapping/data/gc_content/stringent_Tdactyloides_maize_homolog_pairs_gc_content.txt 

# perl get_gc_content_along_transcript_length_for_tripsacum_proteome_genes.pl /Users/cg449/Desktop/tripsacummapping/data/gc_content/gc_content_by_absolute_transcript_length_Tfloridanum_of_stringent_agpv4_homolog_pairs.txt /Users/cg449/Desktop/tripsacummapping/data/gc_content/gc_content_by_absolute_transcript_length_stringent_Tfloridanum_homologs_proteome.txt /Users/cg449/Desktop/tripsacummapping/data/gc_content/gc_content_by_absolute_transcript_length_stringent_Tfloridanum_homologs_non_proteome.txt /Users/cg449/Desktop/tripsacummapping/data/gc_content/stringent_Tfloridanum_maize_homolog_pairs_gc_content.txt 


# Read in proteome genes from AGPv4:

open(REF, "</Users/cg449/Desktop/tripsacummapping/data/gc_content/proteome_geneID_AGPv4.txt") || die "Can't open REF.\n";

my %proteome = ();

while(<REF>) {
	my $line = $_;
	chomp($line);
	$proteome{$line} = 1;
}

close REF;

# Read in maize/tripsacum homolog pairs, made with full-length tripsacum transcripts only

my $file2 = $ARGV[3];
my %homologs = ();

open(HOMOLOGS, "<$file2") || die "Can't open HOMOLOGS.\n";
	
while(<HOMOLOGS>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $maize_gene = $columns[1];
	my $tripsacum_transcript = $columns[0];
	$homologs{$tripsacum_transcript} = $maize_gene;
} 

# parse GC content file into proteome and non-proteome:

my $file = $ARGV[0];
my $proteome_out = $ARGV[1];
my $non_proteome_out = $ARGV[2];

open(FH1, "<$file") || die "Can't open FH1.\n";
open(PROT, ">$proteome_out") || die "Can't open PROT.\n";
open(NON, ">$non_proteome_out") || die "Can't open NON.\n";

while(<FH1>) {
	my $line = $_;
	chomp($line);
	if ($line =~ /^transcript/) {
		print PROT "$line\n";
		print NON "$line\n";
	}

	else {
		my @columns = split(/\s+/, $line);
		my $current_transcript = $columns[0];
		my $current_gene = $homologs{$current_transcript};

		if (defined $proteome{$current_gene}) { # if the gene is in the proteome, print this line to the proteome file
			print PROT "$line\n";
		}

		else { # if the gene is in not the proteome, print this line to the non-proteome file
			print NON "$line\n"; 
		}
	}
}

close FH1;
close PROT;
close NON;


