#!/usr/bin/perl -w
use strict;

# $ARGV[0] should be the list of identifiers
# $ARGV[1] should be the master Fasta file that you want to extract sequences from 
# $ARGV[2] should be the output file


my %ids = ();

use Bio::Perl;
use Bio::SeqIO;

my $file1 = $ARGV[0];
open FH1, "<$file1";

while(<FH1>) {
	my $line = $_;
	chomp($line);
	$ids{$line} = 1;
}

close FH1;

my $file3 = $ARGV[2];
my $seqout = Bio::SeqIO->new(-file=> ">$file3",	-format=> 'fasta');

my $file2 = $ARGV[1];
my $seqio_object = Bio::SeqIO->new(-file => $file2, -format => 'fasta');

while (my $seq_object = $seqio_object->next_seq) {
	my $gene_id = $seq_object->id;
	my $sequence = $seq_object-> seq();
	foreach my $id (keys %ids) {	
		if ($id eq $gene_id) {
			$seqout->write_seq($seq_object);
		}
	}
} 

