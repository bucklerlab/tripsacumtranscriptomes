#!/usr/bin/perl -w

use strict;

# Purpose: to substitute FPKM values from eXpress into the blobplot instead of coverage depth values.
# Usage: perl sub_fpkm_values_in_for_coverage_depth.pl /Users/cg449/Desktop/Tripsacum/transcriptome_assembly/quantify_transcript_abundance/Tdactyloides_eXpress_crown/results.xprs /Users/cg449/Desktop/Tripsacum/transcriptome_assembly/PASA/pasa_Tdactyloides/Tdactyloides2_unaligned_pasa_transcripts_blobplot.txt Tdactyloides2_unaligned_pasa_transcripts_blobplot_FPKM.txt > Tdactyloides2_unaligned_pasa_transcripts_over_50_FPKM.txt

my $file1 = $ARGV[0];
my $file2 = $ARGV[1];
my $file3 = $ARGV[2];
my %fpkms = ();

open FH1, "<$file1" || die "Can't open FH1.\n";

while(<FH1>) {
	my $line = $_;
	chomp($line);
	my @line = split(/\t/, $line);
	my $transcript_id = $line[1];
	my $fpkm = $line[10];
	$fpkms{$transcript_id} = $fpkm; # key: transcriptID, value: fpkm
}

close FH1;

open FH2, "<$file2" || die "Can't open FH2.\n";
open OUT, ">$file3" || die "Can't open OUT.\n";

print "seqid	FPKM	taxlevel_species	taxlevel_genus	taxlevel_family\n";

while(<FH2>) {
	my $line = $_;
	chomp($line);
	my @line = split(/\t/, $line);
	my $transcript = $line[0];
	
	if (exists $fpkms{$transcript}) {
		print OUT "$line[0]	$line[1]	$line[2]	$fpkms{$transcript}	$line[4]	$line[5]	$line[6]\n";

		if ($fpkms{$transcript} > 50) {
			print "$line[0]	$fpkms{$transcript}	$line[4]	$line[5]	$line[6]\n";
		}
	}
}

close FH2;
close OUT;