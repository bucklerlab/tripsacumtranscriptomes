#!/usr/bin/perl -w
use strict;

# This script gets TPM values for all PASA transcript alignments where the transcript aligned over 70% of the length and over 80% sequence identity. It also gets FPKM values for the maize GENE homologs of those Tripsacum transcripts.

my %transcript_to_tpm = ();
my %b73_fpkm = ();

my $file1 = "/Users/cg449/Desktop/Tripsacum/transcriptome_assembly/quantify_transcript_abundance/Tdactyloides_eXpress_leaf/results.xprs";

open(FH1, "<$file1") || die "Can't open FH1.\n";

while(<FH1>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $trip_transcript = $columns[1];
	my $tpm = pop @columns;
	$transcript_to_tpm{$trip_transcript} = $tpm;
}

my $file4 = "/Users/cg449/Desktop/Tripsacum/transcriptome_assembly/quantify_transcript_abundance/B73_v_Tripsacum_expression/Star_pipeline_for_tripsacum/cufflinks/genes.fpkm_tracking_combined";

open(FH4, "<$file4") || die "Can't open FH4.\n";

while(<FH4>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $agpv4 = $columns[0];
	my $fpkm = $columns[1];
	$b73_fpkm{$agpv4} = $fpkm;
}

my $file2 = "/Users/cg449/Desktop/Tripsacum/transcriptome_assembly/PASA/pasa_Tdactyloides/unique_Tdactyloides_transcripts_aligned_to_agpv4_fuzzy_boundaries.txt";

my $file3 = "/Users/cg449/Desktop/tripsacumTranscriptomesData/Tdactyloides_valid_PASA_transcripts_agpv4_homolog_fpkm.txt";

open(FH2, "<$file2") || die "Can't open FH2.\n";

open(OUT1, ">$file3") || die "Can't open OUT1.\n";

print OUT1 "Tdactyloides_transcript	agpv4_gene	Tdactyloides_transcript_tpm	agpv4_gene_fpkm\n";

while(<FH2>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $trip_transcript = shift @columns;
	
	foreach my $agpv4_homolog (@columns) {
	
		if (defined $b73_fpkm{$agpv4_homolog}) {
			print OUT1 "$trip_transcript	$agpv4_homolog	$transcript_to_tpm{$trip_transcript}	$b73_fpkm{$agpv4_homolog}\n";
		}
		
		else {
			print OUT1 "$trip_transcript	$agpv4_homolog	$transcript_to_tpm{$trip_transcript}	NA\n";
		}
	}
	
}
