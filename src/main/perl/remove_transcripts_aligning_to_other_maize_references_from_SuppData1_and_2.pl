#!/usr/bin/perl -w

use strict;

my $td_file = "/Users/cg449/Desktop/tripsacumTranscriptomes/data/pasa_non_maize_against_alternate_references/Td_transcripts_aligned_to_other_inbred_genomes_unique_transcript_names.txt";

my $tf_file = "/Users/cg449/Desktop/tripsacumTranscriptomes/data/pasa_non_maize_against_alternate_references/Tf_transcripts_aligned_to_other_inbred_genomes_unique_transcript_names.txt";

my %td_maize_transcripts = ();
my %tf_maize_transcripts = ();

open(TD, "<$td_file") || die "Can't open TD.\n";

while(<TD>) {
	my $line = $_;
	chomp($line);
	$td_maize_transcripts{$line} = 1;
	print "$line\n";
}

close TD;

open(TF, "<$tf_file") || die "Can't open TF.\n";

while(<TF>) {
	my $line = $_;
	chomp($line);
	$tf_maize_transcripts{$line} = 1;
}

close TF;

my $suppData1 = "/Users/cg449/Desktop/My_manuscripts/de_novo_transcriptome_assembly/PlantGenomeSubmission/SupplementalData1.txt";
my $suppData2 = "/Users/cg449/Desktop/My_manuscripts/de_novo_transcriptome_assembly/PlantGenomeSubmission/SupplementalData2.txt";

my $revised1 = "/Users/cg449/Desktop/My_manuscripts/de_novo_transcriptome_assembly/PlantGenomeSubmission/RevisedSupplementalData1.txt";
my $revised2 = "/Users/cg449/Desktop/My_manuscripts/de_novo_transcriptome_assembly/PlantGenomeSubmission/RevisedSupplementalData2.txt";

open(SD1, "<$suppData1") || die "Can't open SD1.\n";

open(RSD1, ">$revised1") || die "Can't open RSD1.\n";

while(<SD1>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\s+/, $line);
	my $transcript = $columns[0];

	if (defined $td_maize_transcripts{$transcript}) {
		# Don't print out this line to the revised SupplementalData1 file. It aligned to one of the maize genomes.
	} 

	else {
		print RSD1 "$line\n";
	}
}

close SD1;
close RSD1;

open(SD2, "<$suppData2") || die "Can't open SD2.\n";

open(RSD2, ">$revised2") || die "Can't open RSD2.\n";

while(<SD2>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\s+/, $line);
	my $transcript = $columns[0];

	if (defined $tf_maize_transcripts{$transcript}) {
		# Don't print out this line to the revised SupplementalData2 file. It aligned to one of the maize genomes.
	} 

	else {
		print RSD2 "$line\n";
	}
	
}

close SD2;
close RSD2;
