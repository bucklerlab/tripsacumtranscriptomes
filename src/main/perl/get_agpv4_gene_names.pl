#!/usr/bin/perl -w

use strict;

my $file1 = "/Users/cg449/Desktop/B73/AGP_v3/v3_v4_xref.txt";
my %convert = ();

open(FH1, "<$file1") || die "Can't open FH1.\n";

while(<FH1>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $v3 = $columns[0];
	my $v4 = $columns[6];
	$convert{$v3} = $v4;
}

close FH1;

my $file2 = "/Users/cg449/Desktop/B73/AGP_v3/maize1_maize2_homeologous_pairs_agpv3_names.txt";
my $outfile1 = "/Users/cg449/Desktop/tripsacummapping/data/transcriptome_assembly/maize1_maize2_homeologous_pairs_agpv4_names.txt";

open(FH2, "<$file2") || die "Can't open FH2.\n";
open(OUT, ">$outfile1") || die "Can't open OUT.\n";

print OUT "maize1	maize2\n";

while(<FH2>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $maize1 = $columns[0];
	my $maize2 = $columns[1];
	
	print "$maize1$maize2\n";

	if (exists $convert{$maize1}) {
		if (exists $convert{$maize2}) {
			print OUT "$convert{$maize1}	$convert{$maize2}\n";
		}
	}
}

close FH2;
close OUT;