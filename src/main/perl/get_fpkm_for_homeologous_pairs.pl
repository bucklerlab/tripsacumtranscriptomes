#!/usr/bin/perl -w

use strict;

my $file1 = "/Users/cg449/Desktop/Tripsacum/transcriptome_assembly/quantify_transcript_abundance/B73_v_Tripsacum_expression/Star_pipeline_for_tripsacum/cufflinks/genes.fpkm_tracking_combined";

open(FH1, "<$file1") || die "Can't open FH1.\n";

my %b73_fpkms = ();
my %td_fpkms = ();
my %tf_fpkms = ();

while(<FH1>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $gene = $columns[0];
	my $b73 = $columns[1];
	my $td = $columns[2];
	my $tf = $columns[3];
	$b73_fpkms{$gene} = $b73;
	$td_fpkms{$gene} = $td;
	$tf_fpkms{$gene} = $tf;
}

close FH1;

my $file2 = "/Users/cg449/Desktop/tripsacummapping/data/transcriptome_assembly/maize1_maize2_homeologous_pairs_by_proteome.txt";
my $outfile1 = "/Users/cg449/Desktop/tripsacummapping/data/transcriptome_assembly/maize1_maize2_homeologous_pairs_by_proteome_FPKM.txt";

open(FH2, "<$file2") || die "Can't open FH2.\n";
open(OUT1, ">$outfile1") || die "Can't open OUT1.\n";

print OUT1 "maize1_homeolog	maize1_b73_fpkm	maize1_td_fpkm	maize1_tf_fpkm	";
print OUT1 "maize2_homeolog	maize2_b73_fpkm	maize2_td_fpkm	maize2_tf_fpkm	Proteome_or_Nonproteome\n";

while(<FH2>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $maize1_homeolog = $columns[0];
	my $maize2_homeolog = $columns[1];
	my $pn = $columns[2];

	if (defined $b73_fpkms{$maize1_homeolog}) {
		if (defined $b73_fpkms{$maize2_homeolog}) {
			print OUT1 "$maize1_homeolog	$b73_fpkms{$maize1_homeolog}	$td_fpkms{$maize1_homeolog}	$tf_fpkms{$maize1_homeolog}	";
			print OUT1 "$maize2_homeolog	$b73_fpkms{$maize2_homeolog}	$td_fpkms{$maize2_homeolog}	$tf_fpkms{$maize2_homeolog}	$pn\n";
		}

		else {
			print "$maize1_homeolog	$maize2_homeolog\n";
		}
	}

	else {
		print "$maize1_homeolog	$maize2_homeolog\n";
	}

}

close FH2;
close OUT1;

