#!/usr/bin/perl -w

use strict;

my $file1 = "/Users/cg449/Desktop/tripsacummapping/data/gc_content/full-length_Tdactyloides_transcript_agpv4_gene_and_transcript_homolog_best_agpv3_protein_hit.txt";
my $file2 = "/Users/cg449/Desktop/tripsacummapping/data/gc_content/gc_content_by_absolute_transcript_length_agpv4_homologs_of_full-length_Tdactyloides_transcripts.txt"; 
my $file3 = "/Users/cg449/Desktop/tripsacummapping/data/gc_content/gc_content_by_absolute_transcript_length_best_agpv4_transcript_homologs_of_full-length_Tdactyloides_transcripts.txt";

#my $file1 = "/Users/cg449/Desktop/tripsacummapping/data/gc_content/full-length_Tfloridanum_transcript_agpv4_gene_and_transcript_homolog_best_agpv3_protein_hit.txt";
#my $file2 = "/Users/cg449/Desktop/tripsacummapping/data/gc_content/gc_content_by_absolute_transcript_length_agpv4_homologs_of_full-length_Tfloridanum_transcripts.txt";
#my $file3 = "/Users/cg449/Desktop/tripsacummapping/data/gc_content/gc_content_by_absolute_transcript_length_best_agpv4_transcript_homologs_of_full-length_Tfloridanum_transcripts.txt";

open(FH1, "<$file1") || die "Can't open FH1.\n";

my %best_agpv4_transcripts = ();

while(<FH1>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\s+/, $line);
	my $agpv4_transcript = $columns[2];
	$best_agpv4_transcripts{$agpv4_transcript} = 1;
}

close FH1;

open(FH2, "<$file2") || die "Can't open FH2.\n";
open(OUT, ">$file3") || die "Can't open OUT.\n";

while(<FH2>) {
	my $line = $_;
	my @columns = split(/\s+/, $line);
	my $current_transcript = $columns[0];

	if (defined $best_agpv4_transcripts{$current_transcript}) {
		print OUT "$line";
	}
}

close FH2;
