#!/usr/bin/perl -w

use strict;

#my $file1 = "B73_AGPv4.34_genes_categorized_by_subgenome.txt";
#my $file2 = "Tdactyloides_transcripts_with_95_hit_length_aligned_B73_protein_agpv3.txt";
#my $file3 = "unique_Tdactyloides_transcripts_aligned_to_agpv4_fuzzy_boundaries.txt";
#my $file4 = "gc_content_by_absolute_transcript_length_B73_agpv4.txt";
#my $file5 = "gc_content_by_absolute_transcript_length_Tdactyloides.txt";
#my $file6 = "gc_content_by_absolute_transcript_length_agpv4_homologs_of_full-length_Tdactyloides_transcripts.txt"; # outfile
#my $file7 = "gc_content_by_absolute_transcript_length_full-length_Tdactyloides_transcripts.txt"; # outfile
#my $file8 = "maize_homologs_paired_with_full-length_Tdactyloides_transcripts.txt"; #outfile
#my $file9 = "gc_content_for_maize_Tdactyloides_Tfloridanum_calculated_with_full-length_transcripts.txt";
#my $file10 = "gc_content_for_maize_Tdactyloides_Tfloridanum_calculated_with_full-length_transcripts_by_subgenome.txt";

my $file1 = "B73_AGPv4.34_genes_categorized_by_subgenome.txt";
my $file2 = "Tfloridanum_transcripts_with_95_hit_length_aligned_B73_protein_agpv3.txt";
my $file3 = "unique_Tfloridanum_transcripts_aligned_to_agpv4_fuzzy_boundaries.txt";
my $file4 = "gc_content_by_absolute_transcript_length_B73_agpv4.txt";
my $file5 = "gc_content_by_absolute_transcript_length_Tfloridanum.txt";
my $file6 = "gc_content_by_absolute_transcript_length_agpv4_homologs_of_full-length_Tfloridanum_transcripts.txt";
my $file7 = "gc_content_by_absolute_transcript_length_full-length_Tfloridanum_transcripts.txt";
my $file8 = "maize_homologs_paired_with_full-length_Tfloridanum_transcripts.txt";
my $file9 = "gc_content_for_maize_Tdactyloides_Tfloridanum_calculated_with_full-length_transcripts.txt"; 
my $file10 = "gc_content_for_maize_Tdactyloides_Tfloridanum_calculated_with_full-length_transcripts_by_subgenome.txt";

# Read maize genes into %maize_A hash or %maize_B hash

open(FH1, "<$file1") || die "Can't open FH1.\n"; # B73_AGPv4.34_genes_categorized_by_subgenome.txt

my %maize_A = (); # key = gene;
my %maize_B = (); # key = gene;

while(<FH1>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $maize_gene = $columns[0];
	my $last_column = $columns[4];
		
	if ($last_column =~ /Subgenome=A/) {
		$maize_A{$maize_gene} = 0;
	}

	elsif ($last_column =~ /Subgenome=B/) {
		$maize_B{$maize_gene} = 0;
	}

	else {
		print "Gene from subgenome file can't be assigned a subgenome.\n";
	}
}

close FH1;

# Split whole gene GC content file into subgenome A and B

open(FH9, "<$file9") || die "Can't open FH9.\n"; #gc_content_for_maize_Tdactyloides_Tfloridanum_calculated_with_full-length_transcripts.txt
open(OUT10, ">$file10") || die "Can't open OUT10.\n"; #gc_content_for_maize_Tdactyloides_Tfloridanum_calculated_with_full-length_transcripts_by_subgenome.txt

print OUT10 "maize_gene	avg_gc_of_maize_gene	avg_gc_of_Tdactyloides_homolog	avg_gc_of_Tfloridanum_homolog	subgenome\n";

while(<FH9>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $maize_gene = $columns[0];

	if (defined $maize_A{$maize_gene}) {
		print OUT10 "$line	A\n";
	}

	if (defined $maize_B{$maize_gene}) {
		print OUT10 "$line	B\n";
	}
}

close FH9;
close OUT10;

# Store the names of full-length Tripsacum transcripts in the %full_length_Tripsacum_transcripts hash

open(FH2, "<$file2") || die "Can't open FH2.\n"; # Tdactyloides_transcripts_with_95_hit_length_aligned_B73_protein_agpv3.txt 

my %full_length_Tripsacum_transcripts = (); # key: tripsacum transcript, value: maize gene it aligns to

while(<FH2>) {
	my $transcript = $_;
	chomp($transcript);
	$full_length_Tripsacum_transcripts{$transcript} = "zero";
}

close FH2;

# Pair full-length Tripsacum transcripts with the maize gene they align to

open(FH3, "<$file3") || die "Can't open FH3.\n"; # unique_Tdactyloides_transcripts_aligned_to_agpv4_fuzzy_boundaries.txt 

while(<FH3>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $trip_transcript = $columns[0];
	my $maize_gene = $columns[1];

	if (defined $full_length_Tripsacum_transcripts{$trip_transcript}) {
		$full_length_Tripsacum_transcripts{$trip_transcript} = $maize_gene;
	}
}

close FH3;

# Print out gc along absolute length for maize homologs of full-length transcripts

open(FH4, "<$file4") || die "Can't open FH4.\n"; # gc_content_by_absolute_transcript_length_B73_agpv4.txt 
open(OUT6, ">$file6") || die "Can't open FH6.\n"; # gc_content_by_absolute_transcript_length_agpv4_homologs_of_full-length_Tdactyloides_transcripts.txt

while(<FH4>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\s/, $line);
	my $maize_transcript = $columns[0];
	my $i = 0;
	my $maize_gene = "";
	
	if ($maize_transcript =~ /^([^_]+)_\S+/) {
		$maize_gene = $1;
	}

	foreach my $trip_transcript (keys %full_length_Tripsacum_transcripts) {
		if ($full_length_Tripsacum_transcripts{$trip_transcript} eq $maize_gene) {
			if ($i < 1) {
				print OUT6 "$line\n";
			}
			$i++; 
		}
	}	
}

close FH4;

open(FH5, "<$file5") || die "Can't open FH5.\n"; # gc_content_by_absolute_transcript_length_Tdactyloides.txt 
open(OUT7, ">$file7") || die "Can't open FH7.\n"; # gc_content_by_absolute_transcript_length_full-length_Tdactyloides_transcripts.txt

while(<FH5>) {
	my $line = $_;
	chomp($line);
	
	if ($line =~ /^(TRINITY\S+)/) {

	my $trip_transcript = $1;

	if (defined $full_length_Tripsacum_transcripts{$trip_transcript}) {	
		if ($full_length_Tripsacum_transcripts{$trip_transcript} eq "zero") {
			# do nothing
		}

		else {
			print OUT7 "$line\n";
		}
	}
	}
}

close FH5;

open(OUT8, ">$file8") || die "Can't open OUT8.\n"; # maize_homologs_paired_with_full-length_Tdactyloides_transcripts.txt

foreach my $trip_transcript (keys %full_length_Tripsacum_transcripts) {
	if ($full_length_Tripsacum_transcripts{$trip_transcript} eq "zero") {
		# do nothing
	}
	else {
		print OUT8 "$full_length_Tripsacum_transcripts{$trip_transcript}	$trip_transcript\n";
	}
}

close OUT8;
