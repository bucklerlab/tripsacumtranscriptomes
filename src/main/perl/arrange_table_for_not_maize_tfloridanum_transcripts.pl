#!/usr/bin/perl -w

use strict;

# This is just to get a table with the following columns for tripsacum transcripts that aren't present in maize. The columns in the output file will be:
# transcript	gene_description	pfam_id	pfam_desc	closest_species	root_fpkm	crown_fpkm	leaf_fpkm	inflorescence_fpkm
# usage: perl arrange_table_for_not_maize_tfloridanum_transcripts.pl /Users/cg449/Desktop/tripsacummapping/data/transcriptome_assembly/Tripsacum_transcripts_absent_from_B73/Tfloridanum_non-maize_transcripts_fpkm_over_1_transcript_names.txt /Users/cg449/Desktop/tripsacummapping/data/transcriptome_assembly/Tripsacum_transcripts_absent_from_B73/CBSU_analysis/Tfloridanum_non-maize_transcripts_fpkm_over_1.blastx.nr.outfmt6 /Users/cg449/Desktop/tripsacummapping/data/transcriptome_assembly/Tripsacum_transcripts_absent_from_B73/GI_descriptions_from_non_maize_Td_and_Tf_transcripts.txt /Users/cg449/Desktop/tripsacummapping/data/transcriptome_assembly/Tripsacum_transcripts_absent_from_B73/CBSU_analysis/Tfloridanum_non_maize_hmmscan.tblout > Tfloridanum_not_maize_fpkm_over_1_summary.txt


my $file1 = $ARGV[0]; # Tfloridanum_non-maize_transcripts_fpkm_over_1_transcript_names.txt
my $file2 = $ARGV[1]; # Tfloridanum_non-maize_transcripts_fpkm_over_1.blastx.nr.outfmt6
my $file3 = "/Users/cg449/Desktop/Tripsacum/transcriptome_assembly/quantify_transcript_abundance/Tfloridanum_eXpress_root/results.xprs";
my $file4 = "/Users/cg449/Desktop/Tripsacum/transcriptome_assembly/quantify_transcript_abundance/Tfloridanum_eXpress_crown/results.xprs";
my $file5 = "/Users/cg449/Desktop/Tripsacum/transcriptome_assembly/quantify_transcript_abundance/Tfloridanum_eXpress_leaf/results.xprs";
my $file6 = "/Users/cg449/Desktop/Tripsacum/transcriptome_assembly/quantify_transcript_abundance/Tfloridanum_eXpress_inflorescence1/results.xprs";
my $file7 = "/Users/cg449/Desktop/Tripsacum/transcriptome_assembly/quantify_transcript_abundance/Tfloridanum_eXpress_inflorescence2/results.xprs";
my $file8 = "/Users/cg449/Desktop/Tripsacum/transcriptome_assembly/quantify_transcript_abundance/Tfloridanum_eXpress_inflorescence3/results.xprs";
my $file9 = $ARGV[2]; # GI_descriptions_from_non_maize_Td_and_Tf_transcripts.txt
my $file10 = $ARGV[3]; # Tfloridanum_non_maize_hmmscan.tblout
my $file11 = "/Users/cg449/Desktop/tripsacummapping/data/transcriptome_assembly/Tripsacum_transcripts_absent_from_B73/Tfloridanum_non-B73_transcripts_fpkm_over_1_blobplot.txt";
my %not_maize_over_1_fpkm = (); # key: transcript, value: description (gi)

open FH1, "<$file1" || die "Can't open FH1.\n"; # Tfloridanum_non-maize_transcripts_fpkm_over_1_transcript_names.txt

while(<FH1>) {
	my $line = $_;
	chomp($line);
	$not_maize_over_1_fpkm{$line} = "blank";
}

close FH1;

open FH2, "<$file2" || die "Can't open FH2.\n"; # Tfloridanum_non-maize_transcripts_fpkm_over_1.blastx.nr.outfmt6

while(<FH2>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $transcript = $columns[0];
	my $desc = $columns[1];
	my $gi = "";
	my $frame = pop @columns;

	if ($desc =~ /^gi\|[^\|]+\|[^\|]+\|(\S+)\|$/) { 
		$gi = $1;
	}

	if ($frame >0) {
		$not_maize_over_1_fpkm{$transcript} = $gi;
	}
}

close FH2;

my %root_transcripts = ();
my %species_hits = ();

open FH3, "<$file3" || die "Can't open FH3.\n"; # Tfloridanum_eXpress_root/results.xprs

while(<FH3>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $transcript = $columns[1];
	my $fpkm = $columns[10];
	$root_transcripts{$transcript} = $fpkm;
}

close FH3;

my %crown_transcripts = ();

open FH4, "<$file4" || die "Can't open FH4.\n"; # Tfloridanum_eXpress_crown/results.xprs

while(<FH4>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $transcript = $columns[1];
	my $fpkm = $columns[10];
    $crown_transcripts{$transcript} = $fpkm;
}

close FH4;

my %leaf_transcripts = ();

open FH5, "<$file5" || die "Can't open FH5.\n"; # leaf

while(<FH5>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $transcript = $columns[1];
	my $fpkm = $columns[10];
    $leaf_transcripts{$transcript} = $fpkm;
}

close FH5;

my %inflorescence_transcripts = ();

open FH6, "<$file6" || die "Can't open FH6.\n"; # inflorescence

while(<FH6>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $transcript = $columns[1];
	my $fpkm = $columns[10];
    $inflorescence_transcripts{$transcript} = $fpkm;	
}

close FH6;

my %inflorescence2_transcripts = ();

open FH7, "<$file7" || die "Can't open FH7.\n";

while(<FH7>) {
        my $line = $_;
        chomp($line);
        my @columns = split(/\t/, $line);
        my $transcript = $columns[1];
		my $fpkm = $columns[10];
        $inflorescence2_transcripts{$transcript} = $fpkm;
}

close FH7;

my %inflorescence3_transcripts = ();

open FH8, "<$file8" || die "Can't open FH8.\n";

while(<FH8>) {
        my $line = $_;
        chomp($line);
        my @columns = split(/\t/, $line);
        my $transcript = $columns[1];
		my $fpkm = $columns[10];
        $inflorescence3_transcripts{$transcript} = $fpkm;
}

close FH8;

my %gis = ();

open FH9, "<$file9" || die "Can't open FH9.\n"; # GI_descriptions_from_non_maize_Td_and_Tf_transcripts.txt

while(<FH9>) {
	my $line = $_;
	chomp($line);
	if ($line =~ /^(\S+)\s+(.+)$/) {
		my $gi = $1;
		my $gi_description = $2;
		$gis{$gi} = $gi_description;
	}
}

close FH9;

my %pfam_ids = ();
my %pfam_descs = ();

open FH10, "<$file10" || die "Can't open FH10.\n"; # Tfloridanum_non_maize_hmmscan.tblout

while (<FH10>) {
	my $line = $_;
	chomp($line);
	if ($line =~ /^[^#].+\d+\s+\d+\s+\d+\s+(.+)$/) {
		my @columns = split(/\s+/, $line);
		my $transcript = $columns[2];
		my $pfam_id = $columns[1];
		my $pfam_description = $1;
		$pfam_ids{$transcript}{$pfam_id} = 1;
		$pfam_descs{$transcript}{$pfam_description} = 1;
	}
}

close FH10;

open FH11, "<$file11" || die "Can't open FH11.\n"; # Tfloridanum_non-B73_transcripts_fpkm_over_1_blobplot.txt

while(<FH11>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $transcript = $columns[0];
	my $spp_hit = $columns[4];
	$species_hits{$transcript} = $spp_hit;
}

close FH11;

print "transcript	closest_nr_protein_database_BLAST_hit	nr_protein_description	;pfam_id	;pfam_description	species_of_closest_nt_database_hit	root_fpkm	crown_fpkm	leaf_fpkm	inflorescence1_fpkm	inflorescence2_fpkm	inflorescence3_fpkm\n";

foreach my $transcript (keys %not_maize_over_1_fpkm) {
	print "$transcript";

	if ($not_maize_over_1_fpkm{$transcript} eq "blank") {
		print "	No hit in BLAST nr protein database	.";
	}
	
	else {print "	$not_maize_over_1_fpkm{$transcript}	$gis{$not_maize_over_1_fpkm{$transcript}}";}
	
	if (exists $pfam_ids{$transcript}) {
		print "\t";	
		foreach my $pfam_id (keys %{$pfam_ids{$transcript}}) {
			print ";$pfam_id";
		}
	}
	
	else {print "	;No Pfam hit";}
	
	if (exists $pfam_descs{$transcript}) {
		print "\t";
		foreach my $pfam_desc (keys %{$pfam_descs{$transcript}}) {
			print ";$pfam_desc";
		}
	}
	
	else {print "	;.";}

	if (exists $species_hits{$transcript}) {
		print "	$species_hits{$transcript}";
	}
	
	else {print "	No hit in BLAST nt nucleotide database";}
	
	if (exists $root_transcripts{$transcript}) {
		print "	$root_transcripts{$transcript}";
	}
	
	else {print "	error";}
	
	if (exists $crown_transcripts{$transcript}) {
		print "	$crown_transcripts{$transcript}";
	}
	
	else {print "	error";}
		
	if (exists $leaf_transcripts{$transcript}) {
		print "	$leaf_transcripts{$transcript}";
	}
	
	else {print "	error";}
	
	if (exists $inflorescence_transcripts{$transcript}) {
		print "	$inflorescence_transcripts{$transcript}";
	}
	
	else {print "	error";}

	if (exists $inflorescence2_transcripts{$transcript}) {
		print "	$inflorescence2_transcripts{$transcript}";
	}
	
	else {print "	error";}
	
	if (exists $inflorescence3_transcripts{$transcript}) {
		print "	$inflorescence3_transcripts{$transcript}";
	}
	
	else {print "	error";}


	print "\n";
}
