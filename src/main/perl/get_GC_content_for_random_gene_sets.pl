#!/usr/bin/perl -w

use strict;

open(FH1, "</Users/cg449/Desktop/tripsacumTranscriptomes/data/two_sets_of_random_genes.txt") || die "Can't open FH1.\n";

my %rand1 = ();
my %rand2 = ();

while(<FH1>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $transcript = $columns[0];
	my $set = $columns[1];
	
	if ($set == 2) {
		$rand2{$transcript} = 10;
	}
	
	else {
		$rand1{$transcript} = 10;
	}
}

close FH1;

open(FH2, "</Users/cg449/Desktop/tripsacumTranscriptomesData/gc_content_by_absolute_transcript_length_B73_agpv3.txt") || die "Can't open FH2.\n";

open(OUT1, ">/Users/cg449/Desktop/tripsacumTranscriptomesData/GC_content_random_set1.txt") || die "Can't open OUT1.\n";

open(OUT2, ">/Users/cg449/Desktop/tripsacumTranscriptomesData/GC_content_random_set2.txt") || die "Can't open OUT2.\n";

while(<FH2>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\s+/, $line);
	my $transcript = $columns[0];
	
	print "$transcript\n";
	
	if (defined $rand1{$transcript} ) {
		print OUT1 "$line\n";
	}

	if (defined $rand2{$transcript} ) {
		print OUT2 "$line\n";
	}
}

close FH2;
close OUT1;
close OUT2;