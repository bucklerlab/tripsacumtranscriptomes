#!/usr/bin/perl -w

use strict;

#my $infile1 = "stringent_Tdactyloides_maize_homolog_pairs_gc_content.txt";
#my $infile2 = "gc_content_by_absolute_transcript_length_B73_agpv4.txt";
#my $infile3 = "gc_content_by_absolute_transcript_length_Tdactyloides.txt";
#my $outfile1 = "gc_content_by_absolute_transcript_length_agpv4_of_stringent_Tdactyloides_homolog_pairs.txt";
#my $outfile2 = "gc_content_by_absolute_transcript_length_Tdactyloides_of_stringent_agpv4_homolog_pairs.txt";

my $infile1 = "stringent_Tfloridanum_maize_homolog_pairs_gc_content.txt";
my $infile2 = "gc_content_by_absolute_transcript_length_B73_agpv4.txt";
my $infile3 = "gc_content_by_absolute_transcript_length_Tfloridanum.txt";
my $outfile1 = "gc_content_by_absolute_transcript_length_agpv4_of_stringent_Tfloridanum_homolog_pairs.txt";
my $outfile2 = "gc_content_by_absolute_transcript_length_Tfloridanum_of_stringent_agpv4_homolog_pairs.txt";


# For stringent homolog pairs, store the tripsacum transcript name and agpv4 transcript name in hashes
open(FH1, "<$infile1") || die "Can't open FH1.\n";

my %trip_transcripts = ();
my %agpv4_transcripts = ();

while(<FH1>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $trip_transcript = $columns[0];
	my $agpv4_transcript = $columns[2];
	$trip_transcripts{$trip_transcript} = 1;
	$agpv4_transcripts{$agpv4_transcript} = 1;
}

close FH1;

# Go through the agpv4 file with gc content along absolute length of transcript. Pull out the members of stringent homolog pairs.
open(FH2, "<$infile2") || die "Can't open FH2.\n";
open(OUT1, ">$outfile1") || die "Can't open OUT1.\n";

while(<FH2>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\s+/, $line);
	my $current_transcript = $columns[0];

	if (defined $agpv4_transcripts{$current_transcript}) {
		print OUT1 "$line\n";
	}
}

close FH2;
close OUT1;


# Go through the tripsacum file with gc content along absolute length of transcript. Pull out the members of stringent homolog pairs.
open(FH3, "<$infile3") || die "Can't open FH3.\n";
open(OUT2, ">$outfile2") || die "Can't open OUT2.\n";

while(<FH3>) {
	my $line = $_;
        chomp($line);
        my @columns = split(/\s+/, $line);
        my $current_transcript = $columns[0];

	if (defined $trip_transcripts{$current_transcript}) {
		print OUT2 "$line\n";
	}
}

close FH3;
close OUT2;
