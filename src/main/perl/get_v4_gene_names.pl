#!/usr/bin/perl -w

use strict;

open(FH1, "</Users/cg449/Desktop/B73/AGP_v3/v3_v4_xref.txt") || die "Can't open FH1.\n";

my %v3_to_v4 = ();

while(<FH1>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $v3 = $columns[0];
	my $v4 = $columns[6];
	$v3_to_v4{$v3} = $v4;
}

close FH1;

open(FH2, "</Users/cg449/Desktop/tripsacumTranscriptomes/data/high_confidence_singletons_Schnable_2011_RefGen_v3.txt") || die "Can't open FH2.\n";

open(OUT, ">/Users/cg449/Desktop/tripsacumTranscriptomes/data/high_confidence_singletons_Schnable_2011_RefGen_v4.txt") || die "Can't open OUT.\n";

while(<FH2>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $v3 = $columns[0];
	my $subgenome = $columns[1];
	
	if (defined $v3_to_v4{$v3}) {
		print OUT "$v3_to_v4{$v3}	$subgenome\n";
	}
	
	else {
		print "$v3 not defined in v4.\n";
	}
}

close FH2;
close OUT;