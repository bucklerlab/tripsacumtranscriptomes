#!/usr/bin/perl -w

use strict;

my $infile2 = "/Users/cg449/Desktop/tripsacummapping/data/gc_content/full-length_Tdactyloides_transcript_agpv4_gene_and_transcript_homolog_best_agpv3_protein_hit.txt";
my $outfile1 = "/Users/cg449/Desktop/tripsacummapping/data/gc_content/stringent_Tdactyloides_maize_homolog_pairs_gc_content.txt";
my $outfile2 = "/Users/cg449/Desktop/tripsacummapping/data/gc_content/non-stringent_Tdactyloides_maize_homolog_pairs_gc_content.txt";

#my $infile2 = "/Users/cg449/Desktop/tripsacummapping/data/gc_content/full-length_Tfloridanum_transcript_agpv4_gene_and_transcript_homolog_best_agpv3_protein_hit.txt";
#my $outfile1 = "/Users/cg449/Desktop/tripsacummapping/data/gc_content/stringent_Tfloridanum_maize_homolog_pairs_gc_content.txt";
#my $outfile2 = "/Users/cg449/Desktop/tripsacummapping/data/gc_content/non-stringent_Tfloridanum_maize_homolog_pairs_gc_content.txt";

# Read in agpv3 to agpv4 gene ID history:

my $infile1 = "/Users/cg449/Desktop/tripsacummapping/data/gc_content/maize.v3TOv4.geneIDhistory.txt";
my %history = ();

open(FH1, "<$infile1") || die "Can't open FH1.\n";

while(<FH1>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\s+/, $line);
	my $v3 = $columns[0];
	my $v4 = $columns[1];
	$history{$v3}{$v4} = 1;
}

close FH1;

open(FH2, "<$infile2") || die "Can't open FH2.\n";
open(OUT1, ">$outfile1") || die "Can't open OUT1.\n"; # stringent homologs
open(OUT2, ">$outfile2") || die "Can't open OUT2.\n"; # non-stringent homologs

print OUT1 "fully_assembled_tripsacum_transcript	agpv4_gene_homolog	agpv4_best_transcript	agpv3_protein_blast_hit\n";
print OUT2 "fully_assembled_tripsacum_transcript	agpv4_gene_homolog	agpv4_best_transcript	agpv3_protein_blast_hit\n";


while(<FH2>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $agpv4 = $columns[1];
	my $agpv3_protein = $columns[3];
	my $agpv3_gene = "";
	
	if ($agpv3_protein =~ /^(G[^_]+)_\S+/) {
		$agpv3_gene = $1;
	}

	if ($agpv3_protein =~ /^(A[^_]+_FG)P(\S+)/) {
		my $first_part = $1;
		my $second_part = $2;
		$agpv3_gene = "$first_part"."$second_part";
	}

	if (defined $history{$agpv3_gene}{$agpv4}) {
		print OUT1 "$line\n";
	}

	if (not defined $history{$agpv3_gene}{$agpv4}) {
                print OUT2 "$line\n";
        }
}

close FH2;
close OUT1;
close OUT2;
