#!/usr/bin/perl -w

use strict;

#I asked which agpv4 transcript was the best per agpv4 gene for the GC content analysis. It needed to:
#1) align to the same agpv3 protein as it's tripsacum homolog (about 1200 didn't suprisingly)
#2) have the longest alignment length compared to other agpv4 transcripts from the agpv4 gene

#my $trip_file2 = "Trinity_Tdactyloides_with_roots_no_adapters_only_streptophyta_against_B73_protein.outfmt6";
#my $trip_file1 = "maize_homologs_paired_with_full-length_Tdactyloides_transcripts.txt";
#my $trip_file5 = "full-length_Tdactyloides_transcript_agpv4_gene_and_transcript_homolog_best_agpv3_protein_hit.txt";

my $trip_file2 = "Trinity_Tfloridanum_with_roots_no_adapters_only_streptophyta_against_B73_protein.outfmt6";
my $trip_file1 = "maize_homologs_paired_with_full-length_Tfloridanum_transcripts.txt";
my $trip_file5 = "full-length_Tfloridanum_transcript_agpv4_gene_and_transcript_homolog_best_agpv3_protein_hit.txt";

my $maize_file = "Zea_mays.AGPv4.cdna.all_against_AGPv3_protein.outfmt6"; #Tdactyloides_transcripts_with_95_hit_length_aligned_B73_protein_agpv3.txt

my %full_length_trip_transcripts_to_agpv3_protein = (); # Store the tripsacum transcript ids that cover at least 95% of the length of the closest b73 agpv3 protein hit. key: trip transcript. value: best agpv3 protein hit.
my %full_length_trip_transcripts_to_agpv4_homologs = (); # key : trip transcript. value: agpv4 homolog 

open(FH1, "<$trip_file1") || die "Can't open FH1.\n";

while(<FH1>) {
	my $line = $_;
	chomp($line);
	my @columns =split(/\t/, $line);
	my $maize_homolog = $columns[0];
	my $trip_transcript = $columns[1];
	$full_length_trip_transcripts_to_agpv3_protein{$trip_transcript} = 1; # 1 is a placeholder. the eventual value will be the best agpv3 protein hit.
	$full_length_trip_transcripts_to_agpv4_homologs{$trip_transcript} = $maize_homolog;
}

close FH1;

open(FH2, "<$trip_file2") || die "Can't open FH2.\n"; #Trinity_Tdactyloides_with_roots_no_adapters_only_streptophyta_against_B73_protein.outfmt6

while(<FH2>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $query = $columns[0];
	my $hit = $columns[1];

	if (defined $full_length_trip_transcripts_to_agpv3_protein{$query}) {
		$full_length_trip_transcripts_to_agpv3_protein{$query} = $hit;
	}
}

close FH2;

my $file3 = "Zea_mays.AGPv4.cdna.all_against_AGPv3_protein.outfmt6";

my %all_agpv4_transcript_to_agpv3_protein_hits = (); # all agpv4 transcript to agpv3 protein hits, including the crappy ones. key: agpv4 transcript, value: agpv3_protein

open(FH3, "<$file3") || die "Can't open FH3.\n";

while(<FH3>) {
	my $line = $_;
	chomp($line);
	my @columns = split(/\t/, $line);
	my $query = $columns[0];
	my $hit = $columns[1];
	my $len = $columns[3];
	my $len_hit = "$len".";"."$hit";

	if (defined $all_agpv4_transcript_to_agpv3_protein_hits{$query}) {
		# print "$query already was observed. maps to $hit not $agpv4_transcript_to_agpv3_protein_hits{$query}.\n";
	}	

	if (not defined $all_agpv4_transcript_to_agpv3_protein_hits{$query}) {
		$all_agpv4_transcript_to_agpv3_protein_hits{$query} = $len_hit;
	}
}

close FH3;

# Sift through all the agpv4 transcript to agpv3 protein hits so that I choose the transcript that covers the most length of the agpv3 protein

my %agpv4_gene_to_agpv3_protein_to_agpv4_transcript; # key1: agpv4 gene. key2: agpv3 protein that was a blast hit. value: transcript that covered the longest length of that protein.

foreach my $agpv4_transcript (keys %all_agpv4_transcript_to_agpv3_protein_hits) {

	if ($agpv4_transcript =~ /^([^_]+)_[^_]+/) {
		my $agpv4_gene = $1;

		if ($all_agpv4_transcript_to_agpv3_protein_hits{$agpv4_transcript} =~ /([^;]+);([^;]+)/ ) {
			my $length = $1;
			my $hit = $2;
			my $agpv4_transcript_length = "$agpv4_transcript".";"."$length";

			# Store the agpv4 gene, agpv3 protein hit, and the agpv4 transcript/length
			if (not defined $agpv4_gene_to_agpv3_protein_to_agpv4_transcript{$agpv4_gene}{$hit}) {
				$agpv4_gene_to_agpv3_protein_to_agpv4_transcript{$agpv4_gene}{$hit} = $agpv4_transcript_length;
			}

			# If there already was a transcript for this gene that mapped to that agpv3 protein, keep the transcript that had the longer alignment
			if (defined $agpv4_gene_to_agpv3_protein_to_agpv4_transcript{$agpv4_gene}{$hit}) {
				my $old_transcript_length = $agpv4_gene_to_agpv3_protein_to_agpv4_transcript{$agpv4_gene}{$hit};
				if ($old_transcript_length =~ /([^;]+);([^_]+)/) {
					my $old_transcript = $1;
					my $old_transcript_length = $2;

					if ($old_transcript_length > $length) {
						# If the stored transcript had a longer alignment length, keep the stored transcript in the hash
					}

					if ($old_transcript_length < $length) {
						# If the stored transcript had a shorter alignment length than the current transcript, replace the old transcript with the new transcript in the hash
						$agpv4_gene_to_agpv3_protein_to_agpv4_transcript{$agpv4_gene}{$hit} = $agpv4_transcript_length;
					}
				}
			}
		}
	} 
}

# sanity check (passed)
#foreach my $agpv4_gene (keys %agpv4_gene_to_agpv3_protein_to_agpv4_transcript) {
#	foreach my $agpv3_protein (keys %{$agpv4_gene_to_agpv3_protein_to_agpv4_transcript{$agpv4_gene}}) {
#		print "$agpv4_gene	$agpv3_protein	$agpv4_gene_to_agpv3_protein_to_agpv4_transcript{$agpv4_gene}{$agpv3_protein}\n";
#	}
#}

# For each homolog pair (full-length tripsacum transcript and agpv4 maize gene homolog), find the agpv3 protein that the tripsacum transcript aligned to. Get the homologous agpv4 transcript
# that had the longest alignment to the same agpv3 gene 

open(OUT, ">$trip_file5") || die "Can't open OUT.\n";

print OUT "fully_assembled_tripsacum_transcript	agpv4_gene_homolog	agpv4_best_transcript	agpv3_protein_blast_hit\n";

foreach my $full_length_trip (keys %full_length_trip_transcripts_to_agpv4_homologs) {
	my $agpv4_homolog = $full_length_trip_transcripts_to_agpv4_homologs{$full_length_trip};
	my $agpv3_protein = $full_length_trip_transcripts_to_agpv3_protein{$full_length_trip};
	
	if (defined $agpv4_gene_to_agpv3_protein_to_agpv4_transcript{$agpv4_homolog}{$agpv3_protein}) {
		my $agpv4_transcript_length = $agpv4_gene_to_agpv3_protein_to_agpv4_transcript{$agpv4_homolog}{$agpv3_protein}; 
		
		if ($agpv4_transcript_length =~ /([^;]+);[^_]+/) {
			my $best_agpv4_transcript_homolog = $1;
			print OUT "$full_length_trip	$agpv4_homolog	$best_agpv4_transcript_homolog	$agpv3_protein\n";
			print "$agpv4_homolog	$full_length_trip	$agpv3_protein does have best. It is: $best_agpv4_transcript_homolog!\n";
		}

	}

	if (not defined $agpv4_gene_to_agpv3_protein_to_agpv4_transcript{$agpv4_homolog}{$agpv3_protein}) {
		print "$agpv4_homolog	$full_length_trip	$agpv3_protein doesn't have a best agpv4 transcript.\n";
	}
}

close OUT;	
