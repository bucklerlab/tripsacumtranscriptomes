# STAR aligned B73, Tdactyloides, and Tfloridanum reads directly to AGPv4 genome and Cufflinks was used to measure maize gene abundance for AGPv4 genes

proteome_agpv4_genes <- read.table("/Users/cg449/Desktop/tripsacumTranscriptomes/src/main/R/proteome_geneID_AGPv4.txt", header = TRUE)

Cufflinks <- read.table("/Users/cg449/Desktop/tripsacumTranscriptomesData/genes.fpkm_tracking_combined", header = TRUE)

# Tdactyloides:

Cufflinks_B73_v_tdactyloides_no_zeroes <- subset(Cufflinks, Cufflinks$B73_FPKM > 0.01 & Cufflinks$Tdactyloides_FPKM > 0.01)

Cufflinks_B73_v_tdactyloides_no_zeroes$log_B73 <- log10(Cufflinks_B73_v_tdactyloides_no_zeroes$B73_FPKM)

Cufflinks_B73_v_tdactyloides_no_zeroes$log_Tdactyloides <- log10(Cufflinks_B73_v_tdactyloides_no_zeroes$Tdactyloides_FPKM)

# Tfloridanum:

Cufflinks_B73_v_tfloridanum_no_zeroes <- subset(Cufflinks, Cufflinks$B73_FPKM > 0.01 & Cufflinks$Tfloridanum_FPKM > 0.01)

Cufflinks_B73_v_tfloridanum_no_zeroes$log_B73 <- log10(Cufflinks_B73_v_tfloridanum_no_zeroes$B73_FPKM)

Cufflinks_B73_v_tfloridanum_no_zeroes$log_Tfloridanum <- log10(Cufflinks_B73_v_tfloridanum_no_zeroes$Tfloridanum_FPKM)

# dactyloides vs. floridanum:

Cufflinks_Td_v_Tf_no_zeroes <- subset(Cufflinks, Cufflinks$Tdactyloides_FPKM > 0.01 & Cufflinks$Tfloridanum_FPKM > 0.01)

Cufflinks_Td_v_Tf_no_zeroes$log_Td <- log10(Cufflinks_Td_v_Tf_no_zeroes$Tdactyloides_FPKM)

Cufflinks_Td_v_Tf_no_zeroes$log_Tf <- log10(Cufflinks_Td_v_Tf_no_zeroes$Tfloridanum_FPKM)

###########################################
# PROTEOME
# Split Tdactyloides into proteome and non-proteome:

Cufflinks_B73_v_tdactyloides_no_zeroes$proteome <- Cufflinks_B73_v_tdactyloides_no_zeroes$AGPv4_gene %in% proteome_agpv4_genes$proteome_genes

tdactyloides_proteome <- subset(Cufflinks_B73_v_tdactyloides_no_zeroes, Cufflinks_B73_v_tdactyloides_no_zeroes$proteome == TRUE)

tdactyloides_non_proteome <- subset(Cufflinks_B73_v_tdactyloides_no_zeroes, Cufflinks_B73_v_tdactyloides_no_zeroes$proteome == FALSE)

# Split Tfloridanum into proteome and non-proteome:

Cufflinks_B73_v_tfloridanum_no_zeroes$proteome <- Cufflinks_B73_v_tfloridanum_no_zeroes$AGPv4_gene %in% proteome_agpv4_genes$proteome_genes

tfloridanum_proteome <- subset(Cufflinks_B73_v_tfloridanum_no_zeroes, Cufflinks_B73_v_tfloridanum_no_zeroes$proteome == TRUE)

tfloridanum_non_proteome <- subset(Cufflinks_B73_v_tfloridanum_no_zeroes, Cufflinks_B73_v_tfloridanum_no_zeroes$proteome == FALSE)

# Split Td and Tf into proteome and non-proteome:

Cufflinks_Td_v_Tf_no_zeroes$proteome <- Cufflinks_Td_v_Tf_no_zeroes$AGPv4_gene %in% proteome_agpv4_genes$proteome_genes

Td_v_Tf_proteome <- subset(Cufflinks_Td_v_Tf_no_zeroes, Cufflinks_Td_v_Tf_no_zeroes$proteome == TRUE)

Td_v_Tf_non_proteome <- subset(Cufflinks_Td_v_Tf_no_zeroes, Cufflinks_Td_v_Tf_no_zeroes$proteome == FALSE)

############################################################
# Combined proteome vs. non-proteome scatterplot figure

par(mfrow = c(3,3), mar = c(3,3,2,1) + 0.1)
par(cex.main = 1)

# Tdactyloides all
plot(log_Tdactyloides ~ log_B73, xlim = c(-2,5), ylim = c(-2,5), data=Cufflinks_B73_v_tdactyloides_no_zeroes, xlab = "", ylab = "", cex = 0.05)

n <- nrow(Cufflinks_B73_v_tdactyloides_no_zeroes)

title(xlab = expression('log'[10]*'(B73 gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~dactyloides)*' gene FPKM)'), line = 2)

title(main = "All", line = 0.5)

abline(lm(log_Tdactyloides ~ log_B73, data = Cufflinks_B73_v_tdactyloides_no_zeroes))

model101 <- lm(log_Tdactyloides ~ log_B73, data = Cufflinks_B73_v_tdactyloides_no_zeroes)

summary(model101)

rsquarelm2 <- 0.3804
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.56x+0.35", pos=4)

# Tdactyloides proteome
plot(log_Tdactyloides ~ log_B73, xlim = c(-2,5), ylim = c(-2,5), data=tdactyloides_proteome, xlab = "", ylab = "", cex = 0.05)

n <- nrow(tdactyloides_proteome)

title(xlab = expression('log'[10]*'(B73 gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~dactyloides)*' gene FPKM)'), line = 2)

title(main = "Proteome", line = 0.5)

abline(lm(log_Tdactyloides ~ log_B73, data = tdactyloides_proteome))

model102 <- lm(log_Tdactyloides ~ log_B73, data = tdactyloides_proteome)

summary(model102)

rsquarelm2 <- 0.4223
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.48x+0.56", pos=4)

# Tdactyloides non-proteome
plot(log_Tdactyloides ~ log_B73, xlim = c(-2,5), ylim = c(-2,5), data=tdactyloides_non_proteome, xlab = "", ylab = "", cex = 0.05)

n <- nrow(tdactyloides_non_proteome)

title(xlab = expression('log'[10]*'(B73 gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~dactyloides)*' gene FPKM)'), line = 2)

title(main = "Non-proteome", line = 0.5)

abline(lm(log_Tdactyloides ~ log_B73, data = tdactyloides_non_proteome))

model103 <- lm(log_Tdactyloides ~ log_B73, data = tdactyloides_non_proteome)

summary(model103)

rsquarelm2 <- 0.2862
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.49x+0.19", pos=4)

# Tfloridanum all
plot(log_Tfloridanum ~ log_B73, xlim = c(-2,5), ylim = c(-2,5), data=Cufflinks_B73_v_tfloridanum_no_zeroes, xlab = "", ylab = "", cex = 0.05)

n <- nrow(Cufflinks_B73_v_tfloridanum_no_zeroes)

title(xlab = expression('log'[10]*'(B73 gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~floridanum)*' gene FPKM)'), line = 2)

title(main = "All", line = 0.5)

abline(lm(log_Tfloridanum ~ log_B73, data = Cufflinks_B73_v_tfloridanum_no_zeroes))

model104 <- lm(log_Tfloridanum ~ log_B73, data = Cufflinks_B73_v_tfloridanum_no_zeroes)

summary(model104)

rsquarelm2 <- 0.4576
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.62x+0.29", pos=4)

# Tfloridanum proteome
plot(log_Tfloridanum ~ log_B73, xlim = c(-2,5), ylim = c(-2,5), data=tfloridanum_proteome, xlab = "", ylab = "", cex = 0.05)

n <- nrow(tfloridanum_proteome)

title(xlab = expression('log'[10]*'(B73 gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~floridanum)*' gene FPKM)'), line = 2)

title(main = "Proteome", line = 0.5)

abline(lm(log_Tfloridanum ~ log_B73, data = tfloridanum_proteome))

model105 <- lm(log_Tfloridanum ~ log_B73, data = tfloridanum_proteome)

summary(model105)

rsquarelm2 <- 0.4881
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.62x+0.38", pos=4)

# Tfloridanum non-proteome
plot(log_Tfloridanum ~ log_B73, xlim = c(-2,5), ylim = c(-2,5), data=tfloridanum_non_proteome, xlab = "", ylab = "", cex = 0.05)

n <- nrow(tfloridanum_non_proteome)

title(xlab = expression('log'[10]*'(B73 gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~floridanum)*' gene FPKM)'), line = 2)

title(main = "Non-proteome", line = 0.5)

abline(lm(log_Tfloridanum ~ log_B73, data = tfloridanum_non_proteome))

model106 <- lm(log_Tfloridanum ~ log_B73, data = tfloridanum_non_proteome)

summary(model106)

rsquarelm2 <- 0.3728
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.57x+0.18", pos=4)

## Tdactyloides v. Tfloridanum all
plot(log_Td ~ log_Tf, xlim = c(-2,5), ylim = c(-2,5), data=Cufflinks_Td_v_Tf_no_zeroes, xlab = "", ylab = "", cex = 0.05)

n <- nrow(Cufflinks_Td_v_Tf_no_zeroes)

title(xlab = expression('log'[10]*'('*italic(T.~floridanum)*' gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~dactyloides)*' gene FPKM)'), line = 2)

title(main = "All", line = 0.5)

abline(lm(log_Td ~ log_Tf, data = Cufflinks_Td_v_Tf_no_zeroes))

model107 <- lm(log_Td ~ log_Tf, data = Cufflinks_Td_v_Tf_no_zeroes)

summary(model107)

rsquarelm2 <- 0.7713
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.84x+0.16", pos=4)

# Td v. Tf proteome
plot(log_Td ~ log_Tf, xlim = c(-2,5), ylim = c(-2,5), data=Td_v_Tf_proteome, xlab = "", ylab = "", cex = 0.05)

n <- nrow(Td_v_Tf_proteome)

title(xlab = expression('log'[10]*'('*italic(T.~floridanum)*' gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~dactyloides)*' gene FPKM)'), line = 2)

title(main = "Proteome", line = 0.5)

abline(lm(log_Td ~ log_Tf, data = Td_v_Tf_proteome))

model108 <- lm(log_Td ~ log_Tf, data = Td_v_Tf_proteome)

summary(model108)

rsquarelm2 <- 0.7818
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.83x+0.21", pos=4)

# Td vs. Tf non-proteome
plot(log_Td ~ log_Tf, xlim = c(-2,5), ylim = c(-2,5), data=Td_v_Tf_non_proteome, xlab = "", ylab = "", cex = 0.05)

n <- nrow(Td_v_Tf_non_proteome)

title(xlab = expression('log'[10]*'('*italic(T.~floridanum)*' gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~dactyloides)*' gene FPKM)'), line = 2)

title(main = "Non-proteome", line = 0.5)

abline(lm(log_Td ~ log_Tf, data = Td_v_Tf_non_proteome))

model109 <- lm(log_Td ~ log_Tf, data = Td_v_Tf_non_proteome)

summary(model109)

rsquarelm2 <- 0.7207
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.5, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.82x+0.11", pos=4)

#####################################################
#####################################################
#####################################################
# subgenome A vs. subgenome B
#####################################################

subgenomeA <- read.table(file = "/Users/cg449/Desktop/tripsacumTranscriptomesData/B73_AGPv4.34_genes_subgenomeA.txt", header = FALSE)

subgenomeB <- read.table(file = "/Users/cg449/Desktop/tripsacumTranscriptomesData/B73_AGPv4.34_genes_subgenomeB.txt", header = FALSE)

# Partition T. dactyloides genes into subgenomes:

Cufflinks_B73_v_tdactyloides_no_zeroes$subgenomeA <- Cufflinks_B73_v_tdactyloides_no_zeroes$AGPv4_gene %in% subgenomeA$V1

Cufflinks_B73_v_tdactyloides_no_zeroes$subgenomeB <- Cufflinks_B73_v_tdactyloides_no_zeroes$AGPv4_gene %in% subgenomeB$V1

td_subgenomeA <- subset(Cufflinks_B73_v_tdactyloides_no_zeroes, Cufflinks_B73_v_tdactyloides_no_zeroes$subgenomeA == TRUE & Cufflinks_B73_v_tdactyloides_no_zeroes$subgenomeB == FALSE)

td_subgenomeB <- subset(Cufflinks_B73_v_tdactyloides_no_zeroes, Cufflinks_B73_v_tdactyloides_no_zeroes$subgenomeB == TRUE & Cufflinks_B73_v_tdactyloides_no_zeroes$subgenomeA == FALSE)

# Partition T. floridanum genes into subgenomes:

Cufflinks_B73_v_tfloridanum_no_zeroes$subgenomeA <- Cufflinks_B73_v_tfloridanum_no_zeroes$AGPv4_gene %in% subgenomeA$V1

Cufflinks_B73_v_tfloridanum_no_zeroes$subgenomeB <- Cufflinks_B73_v_tfloridanum_no_zeroes$AGPv4_gene %in% subgenomeB$V1

tf_subgenomeA <- subset(Cufflinks_B73_v_tfloridanum_no_zeroes, Cufflinks_B73_v_tfloridanum_no_zeroes$subgenomeA == TRUE & Cufflinks_B73_v_tfloridanum_no_zeroes$subgenomeB == FALSE)

tf_subgenomeB <- subset(Cufflinks_B73_v_tfloridanum_no_zeroes, Cufflinks_B73_v_tfloridanum_no_zeroes$subgenomeB == TRUE & Cufflinks_B73_v_tfloridanum_no_zeroes$subgenomeA == FALSE)

# Partition T.dactyloides/T.floridanum genes into subgenomes:

Cufflinks_Td_v_Tf_no_zeroes$subgenomeA <- Cufflinks_Td_v_Tf_no_zeroes$AGPv4_gene %in% subgenomeA$V1

Cufflinks_Td_v_Tf_no_zeroes$subgenomeB <- Cufflinks_Td_v_Tf_no_zeroes$AGPv4_gene %in% subgenomeB$V1

tf_td_subgenomeA <- subset(Cufflinks_Td_v_Tf_no_zeroes, Cufflinks_Td_v_Tf_no_zeroes$subgenomeA == TRUE & Cufflinks_Td_v_Tf_no_zeroes$subgenomeB == FALSE)

tf_td_subgenomeB <- subset(Cufflinks_Td_v_Tf_no_zeroes, Cufflinks_Td_v_Tf_no_zeroes$subgenomeB == TRUE & Cufflinks_Td_v_Tf_no_zeroes$subgenomeA == FALSE)


############################################################
# Combined subgenome A vs. subgenome B scatterplot figure

par(mfrow = c(3,3), mar = c(3,3,2,1) + 0.1)
par(cex.main = 1)

# Tdactyloides all
plot(log_Tdactyloides ~ log_B73, xlim = c(-2,5), ylim = c(-2,5), data=Cufflinks_B73_v_tdactyloides_no_zeroes, xlab = "", ylab = "", cex = 0.05)

n <- nrow(Cufflinks_B73_v_tdactyloides_no_zeroes)

title(xlab = expression('log'[10]*'(B73 gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~dactyloides)*' gene FPKM)'), line = 2)

title(main = "All", line = 0.5)

abline(lm(log_Tdactyloides ~ log_B73, data = Cufflinks_B73_v_tdactyloides_no_zeroes))

model101 <- lm(log_Tdactyloides ~ log_B73, data = Cufflinks_B73_v_tdactyloides_no_zeroes)

summary(model101)

rsquarelm2 <- 0.3804
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.56x+0.35", pos=4)

# Tdactyloides subgenome A
plot(log_Tdactyloides ~ log_B73, xlim = c(-2,5), ylim = c(-2,5), data=td_subgenomeA, xlab = "", ylab = "", cex = 0.05)

n <- nrow(td_subgenomeA)

title(xlab = expression('log'[10]*'(B73 gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~dactyloides)*' gene FPKM)'), line = 2)

title(main = "Subgenome A", line = 0.5)

abline(lm(log_Tdactyloides ~ log_B73, data = td_subgenomeA))

model202 <- lm(log_Tdactyloides ~ log_B73, data = td_subgenomeA)

summary(model202)

rsquarelm2 <- 0.3803
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.56x+0.36", pos=4)

# Tdactyloides subgenome B
plot(log_Tdactyloides ~ log_B73, xlim = c(-2,5), ylim = c(-2,5), data=td_subgenomeB, xlab = "", ylab = "", cex = 0.05)

n <- nrow(td_subgenomeB)

title(xlab = expression('log'[10]*'(B73 gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~dactyloides)*' gene FPKM)'), line = 2)

title(main = "Subgenome B", line = 0.5)

abline(lm(log_Tdactyloides ~ log_B73, data = td_subgenomeB))

model203 <- lm(log_Tdactyloides ~ log_B73, data = td_subgenomeB)

summary(model203)

rsquarelm2 <- 0.3981
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.57x+0.35", pos=4)

# Tfloridanum all
plot(log_Tfloridanum ~ log_B73, xlim = c(-2,5), ylim = c(-2,5), data=Cufflinks_B73_v_tfloridanum_no_zeroes, xlab = "", ylab = "", cex = 0.05)

n <- nrow(Cufflinks_B73_v_tfloridanum_no_zeroes)

title(xlab = expression('log'[10]*'(B73 gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~floridanum)*' gene FPKM)'), line = 2)

title(main = "All", line = 0.5)

abline(lm(log_Tfloridanum ~ log_B73, data = Cufflinks_B73_v_tfloridanum_no_zeroes))

model104 <- lm(log_Tfloridanum ~ log_B73, data = Cufflinks_B73_v_tfloridanum_no_zeroes)

summary(model104)

rsquarelm2 <- 0.4576
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.62x+0.29", pos=4)

# Tfloridanum subgenome A
plot(log_Tfloridanum ~ log_B73, xlim = c(-2,5), ylim = c(-2,5), data=tf_subgenomeA, xlab = "", ylab = "", cex = 0.05)

n <- nrow(tf_subgenomeA)

title(xlab = expression('log'[10]*'(B73 gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~floridanum)*' gene FPKM)'), line = 2)

title(main = "Subgenome A", line = 0.5)

abline(lm(log_Tfloridanum ~ log_B73, data = tf_subgenomeA))

model205 <- lm(log_Tfloridanum ~ log_B73, data = tf_subgenomeA)

summary(model205)

rsquarelm2 <- 0.4545
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.62x+0.30", pos=4)


# Tfloridanum subgenome B
plot(log_Tfloridanum ~ log_B73, xlim = c(-2,5), ylim = c(-2,5), data=tf_subgenomeB, xlab = "", ylab = "", cex = 0.05)

n <- nrow(tf_subgenomeB)

title(xlab = expression('log'[10]*'(B73 gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~floridanum)*' gene FPKM)'), line = 2)

title(main = "Subgenome B", line = 0.5)

abline(lm(log_Tfloridanum ~ log_B73, data = tf_subgenomeB))

model206 <- lm(log_Tfloridanum ~ log_B73, data = tf_subgenomeB)

summary(model206)

rsquarelm2 <- 0.4709
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.62x+0.29", pos=4)

## Tdactyloides v. Tfloridanum all
plot(log_Td ~ log_Tf, xlim = c(-2,5), ylim = c(-2,5), data=Cufflinks_Td_v_Tf_no_zeroes, xlab = "", ylab = "", cex = 0.05)

n <- nrow(Cufflinks_Td_v_Tf_no_zeroes)

title(xlab = expression('log'[10]*'('*italic(T.~floridanum)*' gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~dactyloides)*' gene FPKM)'), line = 2)

title(main = "All", line = 0.5)

abline(lm(log_Td ~ log_Tf, data = Cufflinks_Td_v_Tf_no_zeroes))

model107 <- lm(log_Td ~ log_Tf, data = Cufflinks_Td_v_Tf_no_zeroes)

summary(model107)

rsquarelm2 <- 0.7713
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.84x+0.16", pos=4)

# Td v. Tf subgenome A
plot(log_Td ~ log_Tf, xlim = c(-2,5), ylim = c(-2,5), data=tf_td_subgenomeA, xlab = "", ylab = "", cex = 0.05)

n <- nrow(tf_td_subgenomeA)

title(xlab = expression('log'[10]*'('*italic(T.~floridanum)*' gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~dactyloides)*' gene FPKM)'), line = 2)

title(main = "Subgenome A", line = 0.5)

abline(lm(log_Td ~ log_Tf, data = tf_td_subgenomeA))

model208 <- lm(log_Td ~ log_Tf, data = tf_td_subgenomeA)

summary(model208)

rsquarelm2 <- 0.7654
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.83x+0.16", pos=4)


# Td vs. Tf subgenome B
plot(log_Td ~ log_Tf, xlim = c(-2,5), ylim = c(-2,5), data=tf_td_subgenomeB, xlab = "", ylab = "", cex = 0.05)

n <- nrow(tf_td_subgenomeB)

title(xlab = expression('log'[10]*'('*italic(T.~floridanum)*' gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~dactyloides)*' gene FPKM)'), line = 2)

title(main = "Subgenome B", line = 0.5)

abline(lm(log_Td ~ log_Tf, data = tf_td_subgenomeB))

model209 <- lm(log_Td ~ log_Tf, data = tf_td_subgenomeB)

summary(model209)

rsquarelm2 <- 0.7852
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.85x+0.15", pos=4)








































#####################################################
#####################################################
#####################################################
# GO terms for critical biological pathways vs
# all other genes
#####################################################

conserved_goids <- read.table(file = "/Users/cg449/Desktop/tripsacumTranscriptomes/data/B73_AGPv4_genes_with_conserved_GOIDs.txt", header = FALSE)

conserved_goids <- unique(conserved_goids)

names(conserved_goids) <- c("AGPv4_gene")

nonconserved_goids <- read.table(file = "/Users/cg449/Desktop/tripsacumTranscriptomes/data/B73_AGPv4_genes_with_nonconserved_GOIDs.txt", header = FALSE)

nonconserved_goids <- unique(nonconserved_goids)

names(nonconserved_goids) <- c("AGPv4_gene")

# Partition T. dactyloides genes into conserved goids and nonconserved goids:

Cufflinks_B73_v_tdactyloides_no_zeroes$conserved_goids <- Cufflinks_B73_v_tdactyloides_no_zeroes$AGPv4_gene %in% conserved_goids$AGPv4_gene

Cufflinks_B73_v_tdactyloides_no_zeroes$nonconserved_goids <- Cufflinks_B73_v_tdactyloides_no_zeroes$AGPv4_gene %in% nonconserved_goids$AGPv4_gene

td_conserved_goids <- subset(Cufflinks_B73_v_tdactyloides_no_zeroes, Cufflinks_B73_v_tdactyloides_no_zeroes$conserved_goids == TRUE & Cufflinks_B73_v_tdactyloides_no_zeroes$nonconserved_goids == FALSE)

td_nonconserved_goids <- subset(Cufflinks_B73_v_tdactyloides_no_zeroes, Cufflinks_B73_v_tdactyloides_no_zeroes$nonconserved_goids == TRUE & Cufflinks_B73_v_tdactyloides_no_zeroes$conserved_goids == FALSE)

# Partition T. floridanum genes into conserved goids and nonconserved goids:

Cufflinks_B73_v_tfloridanum_no_zeroes$conserved_goids <- Cufflinks_B73_v_tfloridanum_no_zeroes$AGPv4_gene %in% conserved_goids$AGPv4_gene

Cufflinks_B73_v_tfloridanum_no_zeroes$nonconserved_goids <- Cufflinks_B73_v_tfloridanum_no_zeroes$AGPv4_gene %in% nonconserved_goids$AGPv4_gene

tf_conserved_goids <- subset(Cufflinks_B73_v_tfloridanum_no_zeroes, Cufflinks_B73_v_tfloridanum_no_zeroes$conserved_goids == TRUE & Cufflinks_B73_v_tfloridanum_no_zeroes$nonconserved_goids == FALSE)

tf_nonconserved_goids <- subset(Cufflinks_B73_v_tfloridanum_no_zeroes, Cufflinks_B73_v_tfloridanum_no_zeroes$nonconserved_goids == TRUE & Cufflinks_B73_v_tfloridanum_no_zeroes$conserved_goids == FALSE)

# Partition T. dactyloides and T. floridanum genes into conserved goids and nonconserved goids:

Cufflinks_Td_v_Tf_no_zeroes$conserved_goids <- Cufflinks_Td_v_Tf_no_zeroes$AGPv4_gene %in% conserved_goids$AGPv4_gene

Cufflinks_Td_v_Tf_no_zeroes$nonconserved_goids <- Cufflinks_Td_v_Tf_no_zeroes$AGPv4_gene %in% nonconserved_goids$AGPv4_gene

tf_td_conserved_goids <- subset(Cufflinks_Td_v_Tf_no_zeroes, Cufflinks_Td_v_Tf_no_zeroes$conserved_goids == TRUE & Cufflinks_Td_v_Tf_no_zeroes$nonconserved_goids == FALSE)

tf_td_nonconserved_goids <- subset(Cufflinks_Td_v_Tf_no_zeroes, Cufflinks_Td_v_Tf_no_zeroes$nonconserved_goids == TRUE & Cufflinks_Td_v_Tf_no_zeroes$conserved_goids == FALSE)


############################################################
# Combined conserved go ids vs nonconserved go ids scatterplot figure

par(mfrow = c(3,3), mar = c(3,3,2,1) + 0.1)
par(cex.main = 1)

# Tdactyloides all
plot(log_Tdactyloides ~ log_B73, xlim = c(-2,5), ylim = c(-2,5), data=Cufflinks_B73_v_tdactyloides_no_zeroes, xlab = "", ylab = "", cex = 0.05)

n < nrow(Cufflinks_B73_v_tdactyloides_no_zeroes)

title(xlab = expression('log'[10]*'(B73 gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~dactyloides)*' gene FPKM)'), line = 2)

title(main = "All", line = 0.5)

abline(lm(log_Tdactyloides ~ log_B73, data = Cufflinks_B73_v_tdactyloides_no_zeroes))

model101 <- lm(log_Tdactyloides ~ log_B73, data = Cufflinks_B73_v_tdactyloides_no_zeroes)

summary(model101)

rsquarelm2 <- 0.3804
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.56x+0.35", pos=4)

# Tdactyloides conserved go ids
plot(log_Tdactyloides ~ log_B73, xlim = c(-2,5), ylim = c(-2,5), data=td_conserved_goids, xlab = "", ylab = "", cex = 0.05)

n <- nrow(td_conserved_goids)

title(xlab = expression('log'[10]*'(B73 gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~dactyloides)*' gene FPKM)'), line = 2)

title(main = "Core functional\n pathways", line = 0.5)

abline(lm(log_Tdactyloides ~ log_B73, data = td_conserved_goids))

model403 <- lm(log_Tdactyloides ~ log_B73, data = td_conserved_goids)

summary(model403)

rsquarelm2 <- 0.4969
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.68x+0.35", pos=4)

# Tdactyloides stress pathways
plot(log_Tdactyloides ~ log_B73, xlim = c(-2,5), ylim = c(-2,5), data=td_nonconserved_goids, xlab = "", ylab = "", cex = 0.05)

n <- nrow(td_nonconserved_goids)

title(xlab = expression('log'[10]*'(B73 gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~dactyloides)*' gene FPKM)'), line = 2)

title(main = "Stress pathways", line = 0.5)

abline(lm(log_Tdactyloides ~ log_B73, data = td_nonconserved_goids))

model402 <- lm(log_Tdactyloides ~ log_B73, data = td_nonconserved_goids)

summary(model402)

rsquarelm2 <- 0.3423
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.52x+0.42", pos=4)

# Tfloridanum all
plot(log_Tfloridanum ~ log_B73, xlim = c(-2,5), ylim = c(-2,5), data=Cufflinks_B73_v_tfloridanum_no_zeroes, xlab = "", ylab = "", cex = 0.05)

n <- nrow(Cufflinks_B73_v_tfloridanum_no_zeroes)

title(xlab = expression('log'[10]*'(B73 gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~floridanum)*' gene FPKM)'), line = 2)

title(main = "All", line = 0.5)

abline(lm(log_Tfloridanum ~ log_B73, data = Cufflinks_B73_v_tfloridanum_no_zeroes))

model104 <- lm(log_Tfloridanum ~ log_B73, data = Cufflinks_B73_v_tfloridanum_no_zeroes)

summary(model104)

rsquarelm2 <- 0.4576
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.62x+0.29", pos=4)

# Tfloridanum conserved go ids
plot(log_Tfloridanum ~ log_B73, xlim = c(-2,5), ylim = c(-2,5), data=tf_conserved_goids, xlab = "", ylab = "", cex = 0.05)

n <- nrow(tf_conserved_goids)

title(xlab = expression('log'[10]*'(B73 gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~floridanum)*' gene FPKM)'), line = 2)

title(main = "Core functional\n pathways", line = 0.5)

abline(lm(log_Tfloridanum ~ log_B73, data = tf_conserved_goids))

model406 <- lm(log_Tfloridanum ~ log_B73, data = tf_conserved_goids)

summary(model406)

rsquarelm2 <- 0.5461
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.5, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.72x+0.28", pos=4)

# Tfloridanum stress pathways
plot(log_Tfloridanum ~ log_B73, xlim = c(-2,5), ylim = c(-2,5), data=tf_nonconserved_goids, xlab = "", ylab = "", cex = 0.05)

n <- nrow(tf_nonconserved_goids)

title(xlab = expression('log'[10]*'(B73 gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~floridanum)*' gene FPKM)'), line = 2)

title(main = "Stress pathways", line = 0.5)

abline(lm(log_Tfloridanum ~ log_B73, data = tf_nonconserved_goids))

model405 <- lm(log_Tfloridanum ~ log_B73, data = tf_nonconserved_goids)

summary(model405)

rsquarelm2 <- 0.403
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.58x+0.37", pos=4)

## Tdactyloides v. Tfloridanum all
plot(log_Td ~ log_Tf, xlim = c(-2,5), ylim = c(-2,5), data=Cufflinks_Td_v_Tf_no_zeroes, xlab = "", ylab = "", cex = 0.05)

n <- nrow(Cufflinks_Td_v_Tf_no_zeroes)

title(xlab = expression('log'[10]*'('*italic(T.~floridanum)*' gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~dactyloides)*' gene FPKM)'), line = 2)

title(main = "All", line = 0.5)

abline(lm(log_Td ~ log_Tf, data = Cufflinks_Td_v_Tf_no_zeroes))

model107 <- lm(log_Td ~ log_Tf, data = Cufflinks_Td_v_Tf_no_zeroes)

summary(model107)

rsquarelm2 <- 0.7713
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.84x+0.16", pos=4)


# Td vs. Tf conserved go ids
plot(log_Td ~ log_Tf, xlim = c(-2,5), ylim = c(-2,5), data=tf_td_conserved_goids, xlab = "", ylab = "", cex = 0.05)

n <- nrow(tf_td_conserved_goids)

title(xlab = expression('log'[10]*'('*italic(T.~floridanum)*' gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~dactyloides)*' gene FPKM)'), line = 2)

title(main = "Core functional\n pathways", line = 0.5)

abline(lm(log_Td ~ log_Tf, data = tf_td_conserved_goids))

model409 <- lm(log_Td ~ log_Tf, data = tf_td_conserved_goids)

summary(model409)

rsquarelm2 <- 0.8323
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.87x+0.15", pos=4)

# Td v. Tf stress pathways
plot(log_Td ~ log_Tf, xlim = c(-2,5), ylim = c(-2,5), data=tf_td_nonconserved_goids, xlab = "", ylab = "", cex = 0.05)

n <- nrow(tf_td_nonconserved_goids)

title(xlab = expression('log'[10]*'('*italic(T.~floridanum)*' gene FPKM)'), ylab = expression('log'[10]*'('*italic(T.~dactyloides)*' gene FPKM)'), line = 2)

title(main = "Stress pathways", line = 0.5)

abline(lm(log_Td ~ log_Tf, data = tf_td_nonconserved_goids))

model408 <- lm(log_Td ~ log_Tf, data = tf_td_nonconserved_goids)

summary(model408)

rsquarelm2 <- 0.7502
text(-2.3,4.8, bquote(italic(R)^2 == .(round(rsquarelm2, 4))), pos = 4)
text(-2.3,4.1, labels = bquote(italic(p)*' value < 2.2e-16'), pos = 4)
text(2,-1.6, paste0("n=", n), pos = 4)
text(2,-0.9,"y=0.82x+0.18", pos=4)










# final "joyplots"

new_cufflinks <- read.table("/Users/cg449/Desktop/tripsacumTranscriptomesData/genes.fpkm_tracking_combined", header = TRUE)

new_cufflinks$proteome <- new_cufflinks$AGPv4_gene %in% proteome_agpv4_genes$proteome_genes

new_cufflinks$subgenomeA <- new_cufflinks$AGPv4_gene %in% subgenomeA$V1

new_cufflinks$subgenomeB <- new_cufflinks$AGPv4_gene %in% subgenomeB$V1

new_cufflinks$conserved_goids <- new_cufflinks$AGPv4_gene %in% conserved_goids$AGPv4_gene

new_cufflinks$nonconserved_goids <- new_cufflinks$AGPv4_gene %in% nonconserved_goids$AGPv4_gene

new_cufflinks$log_B73 <- log10(new_cufflinks$B73_FPKM)

new_cufflinks$log_Td <- log10(new_cufflinks$Tdactyloides_FPKM)

new_cufflinks$log_Tf <- log10(new_cufflinks$Tfloridanum_FPKM)

B73_no_zeroes <- subset(new_cufflinks, new_cufflinks$B73_FPKM > 0.01)

Td_no_zeroes <- subset(new_cufflinks, new_cufflinks$Tdactyloides_FPKM > 0.01)

Tf_no_zeroes <- subset(new_cufflinks, new_cufflinks$Tfloridanum_FPKM > 0.01)

# All genes joyplots

par(mfrow = c(4,1), mar = c(0.1,2,0.5,0.1) + 0.1)

x <- density(B73_no_zeroes$log_B73)
xn <- nrow(B73_no_zeroes)
plot(x, main = "", xaxt = "n", xlim = c(-2,4), ylim = c(0,0.65), bty = "n", bty = "n")
polygon(x, col = "blue")
abline(v=mean(B73_no_zeroes$log_B73), lwd = 3)
text(3,0.1, paste0("n=", xn), pos = 4)

y <- density(Td_no_zeroes$log_Td)
yn <- nrow(Td_no_zeroes)
plot(y, main = "", xaxt = "n", xlim = c(-2,4), ylim = c(0,0.65), bty = "n", bty = "n")
polygon(y, col = "blue")
abline(v=mean(Td_no_zeroes$log_Td), lwd = 3)
text(3,0.1, paste0("n=", yn), pos = 4)

z <- density(Tf_no_zeroes$log_Tf)
zn <- nrow(Tf_no_zeroes)
plot(z, main = "", xlab = "Log(FPKM)", xlim = c(-2,4), ylim = c(0,0.65), bty = "n", bty = "n")
polygon(z, col = "blue")
abline(v=mean(Tf_no_zeroes$log_Tf), lwd = 3)
text(3,0.1, paste0("n=", zn), pos = 4)


# Proteome joyplots

b73_p <- subset(B73_no_zeroes, B73_no_zeroes$proteome == TRUE)
b73_n <- subset(B73_no_zeroes, B73_no_zeroes$proteome == FALSE)

td_p <- subset(Td_no_zeroes, Td_no_zeroes$proteome == TRUE)
td_n <- subset(Td_no_zeroes, Td_no_zeroes$proteome == FALSE)

tf_p <- subset(Tf_no_zeroes, Tf_no_zeroes$proteome == TRUE)
tf_n <- subset(Tf_no_zeroes, Tf_no_zeroes$proteome == FALSE)

par(mfrow = c(7,1), mar = c(0.1,2,0.5,0.1) + 0.1)

a <- density(b73_p$log_B73)
an <- nrow(b73_p)
plot(a, main = "", xaxt = "n", xlim = c(-2,4), ylim = c(0,0.65), bty = "n", bty = "n")
polygon(a, col = "turquoise")
abline(v=mean(b73_p$log_B73), lwd = 3)
text(3,0.1, paste0("n=", an), pos = 4)

b <- density(b73_n$log_B73)
bn <- nrow(b73_n)
plot(b, main = "", xaxt = "n", xlim = c(-2,4), ylim = c(0,0.65), bty = "n")
polygon(b, col = "salmon")
abline(v=mean(b73_n$log_B73), lwd = 3)
text(3,0.1, paste0("n=", bn), pos = 4)

e <- density(td_p$log_Td)
en <- nrow(td_p)
plot(e, main = "", xaxt = "n", xlim = c(-2,4), ylim = c(0,0.65), bty = "n")
polygon(e, col = "turquoise")
abline(v=mean(td_p$log_Td), lwd = 3)
text(3,0.1, paste0("n=", en), pos = 4)

f <- density(td_n$log_Td)
fn <- nrow(td_n)
plot(f, main = "", xaxt = "n", xlim = c(-2,4), ylim = c(0,0.65), bty = "n")
polygon(f, col = "salmon")
abline(v=mean(td_n$log_Td), lwd = 3)
text(3,0.1, paste0("n=", fn), pos = 4)

g <- density(tf_p$log_Tf)
gn <- nrow(tf_p)
plot(g, main = "", xaxt = "n", xlim = c(-2,4), ylim = c(0,0.65), bty = "n")
polygon(g, col = "turquoise")
abline(v=mean(tf_p$log_Tf), lwd = 3)
text(3,0.1, paste0("n=", gn), pos = 4)

h <- density(tf_n$log_Tf)
hn <- nrow(tf_n)
plot(h, main = "", xlim = c(-2,4), ylim = c(0,0.65), bty = "n", xlab = "Log(FPKM)")
polygon(h, col = "salmon")
abline(v=mean(tf_n$log_Tf), lwd = 3)
text(3,0.1, paste0("n=", hn), pos = 4)

# ttests

t.test(b73_p$log_B73, b73_n$log_B73)
t.test(b73_p$B73_FPKM, b73_n$B73_FPKM)

t.test(td_p$log_Td, td_n$log_Td)

t.test(tf_p$log_Tf, tf_n$log_Tf)

# Subgenome joyplots

b73_a <- subset(B73_no_zeroes, B73_no_zeroes$subgenomeA == TRUE)
b73_b <- subset(B73_no_zeroes, B73_no_zeroes$subgenomeB == TRUE)

td_a <- subset(Td_no_zeroes, Td_no_zeroes$subgenomeA == TRUE)
td_b <- subset(Td_no_zeroes, Td_no_zeroes$subgenomeB == TRUE)

tf_a <- subset(Tf_no_zeroes, Tf_no_zeroes$subgenomeA == TRUE)
tf_b <- subset(Tf_no_zeroes, Tf_no_zeroes$subgenomeB == TRUE)

t.test(b73_a$log_B73, b73_b$log_B73)
t.test(b73_a$B73_FPKM, b73_b$B73_FPKM)
t.test(td_a$log_Td, td_b$log_Td)
t.test(td_a$Tdactyloides_FPKM, td_b$Tdactyloides_FPKM)
t.test(tf_a$log_Tf, tf_b$log_Tf)
t.test(tf_a$Tfloridanum_FPKM, tf_b$Tfloridanum_FPKM)

par(mfrow = c(7,1), mar = c(0.1,2,0.5,0.1) + 0.1)

a2 <- density(b73_a$log_B73)
a2n <- nrow(b73_a)
plot(a2, main = "", xaxt = "n", xlim = c(-2,4), ylim = c(0,0.65), bty = "n", bty = "n")
polygon(a2, col = "purple1")
abline(v=mean(b73_a$log_B73), lwd = 3)
text(3,0.1, paste0("n=", a2n), pos = 4)

b2 <- density(b73_b$log_B73)
b2n <- nrow(b73_b)
plot(b2, main = "", xaxt = "n", xlim = c(-2,4), ylim = c(0,0.65), bty = "n")
polygon(b2, col = "yellow")
abline(v=mean(b73_b$log_B73), lwd = 3)
text(3,0.1, paste0("n=", b2n), pos = 4)

e2 <- density(td_a$log_Td)
e2n <- nrow(td_a)
plot(e2, main = "", xaxt = "n", xlim = c(-2,4), ylim = c(0,0.65), bty = "n")
polygon(e2, col = "purple1")
abline(v=mean(td_a$log_Td), lwd = 3)
text(3,0.1, paste0("n=", e2n), pos = 4)

f2 <- density(td_b$log_Td)
f2n <-nrow(td_b)
plot(f2, main = "", xaxt = "n", xlim = c(-2,4), ylim = c(0,0.65), bty = "n")
polygon(f2, col = "yellow")
abline(v=mean(td_b$log_Td), lwd = 3)
text(3,0.1, paste0("n=", f2n), pos = 4)

g2 <- density(tf_a$log_Tf)
g2n <- nrow(tf_a)
plot(g2, main = "", xaxt = "n", xlim = c(-2,4), ylim = c(0,0.65), bty = "n")
polygon(g2, col = "purple1")
abline(v=mean(tf_a$log_Tf), lwd = 3)
text(3,0.1, paste0("n=", g2n), pos = 4)

h2 <- density(tf_b$log_Tf)
h2n <- nrow(tf_b)
plot(h2, main = "", xlim = c(-2,4), ylim = c(0,0.65), bty = "n", xlab = "Log(FPKM)")
polygon(h2, col = "yellow")
abline(v=mean(tf_b$log_Tf), lwd = 3)
text(3,0.1, paste0("n=", h2n), pos = 4)

# Core vs. stress pathway joyplots

b73_core <- subset(B73_no_zeroes, B73_no_zeroes$conserved_goids == TRUE & B73_no_zeroes$nonconserved_goids == FALSE)
b73_stress <- subset(B73_no_zeroes, B73_no_zeroes$nonconserved_goids == TRUE & B73_no_zeroes$conserved_goids == FALSE)

td_core <- subset(Td_no_zeroes, Td_no_zeroes$conserved_goids == TRUE & Td_no_zeroes$nonconserved_goids == FALSE)
td_stress <- subset(Td_no_zeroes, Td_no_zeroes$nonconserved_goids == TRUE & Td_no_zeroes$conserved_goids == FALSE)

tf_core <- subset(Tf_no_zeroes, Tf_no_zeroes$conserved_goids == TRUE & Tf_no_zeroes$nonconserved_goids == FALSE)
tf_stress <- subset(Tf_no_zeroes, Tf_no_zeroes$nonconserved_goids == TRUE & Tf_no_zeroes$conserved_goids == FALSE)

t.test(b73_core$log_B73, b73_stress$log_B73)
t.test(b73_core$B73_FPKM, b73_stress$B73_FPKM)

t.test(td_core$log_Td, td_stress$log_Td)
t.test(td_core$Tdactyloides_FPKM, td_stress$Tdactyloides_FPKM)

t.test(tf_core$log_Tf, tf_stress$log_Tf)
t.test(tf_core$Tfloridanum_FPKM, tf_stress$Tfloridanum_FPKM)

# Get untransformed FPKM values between groups

par(mfrow = c(7,1), mar = c(0.1,2,0.5,0.1) + 0.1)

a3 <- density(b73_core$log_B73)
a3n <- nrow(b73_core)
plot(a3, main = "", xaxt = "n", xlim = c(-2,4), ylim = c(0,0.65), bty = "n", bty = "n")
polygon(a3, col = "green")
abline(v=mean(b73_core$log_B73), lwd = 3)
text(3,0.1, paste0("n=", a3n), pos = 4)

b3 <- density(b73_stress$log_B73)
b3n <- nrow(b73_stress)
plot(b3, main = "", xaxt = "n", xlim = c(-2,4), ylim = c(0,0.65), bty = "n")
polygon(b3, col = "gray")
abline(v=mean(b73_stress$log_B73), lwd = 3)
text(3,0.1, paste0("n=", b3n), pos = 4)

e3 <- density(td_core$log_Td)
e3n <- nrow(td_core)
plot(e3, main = "", xaxt = "n", xlim = c(-2,4), ylim = c(0,0.65), bty = "n")
polygon(e3, col = "green")
abline(v=mean(td_core$log_Td), lwd = 3)
text(3,0.1, paste0("n=", e3n), pos = 4)

f3 <- density(td_stress$log_Td)
f3n <- nrow(td_stress)
plot(f3, main = "", xaxt = "n", xlim = c(-2,4), ylim = c(0,0.65), bty = "n")
polygon(f3, col = "gray")
abline(v=mean(td_stress$log_Td), lwd = 3)
text(3,0.1, paste0("n=", f3n), pos = 4)

g3 <- density(tf_core$log_Tf)
g3n <- nrow(tf_core)
plot(g3, main = "", xaxt = "n", xlim = c(-2,4), ylim = c(0,0.65), bty = "n")
polygon(g3, col = "green")
abline(v=mean(tf_core$log_Tf), lwd = 3)
text(3,0.1, paste0("n=", g3n), pos = 4)

h3 <- density(tf_stress$log_Tf)
h3n <- nrow(tf_stress)
plot(h3, main = "", xlim = c(-2,4), ylim = c(0,0.65), bty = "n", xlab = "Log(FPKM)")
polygon(h3, col = "gray")
abline(v=mean(tf_stress$log_Tf), lwd = 3)
text(3,0.1, paste0("n=", h3n), pos = 4)

# regular old histograms

#par(mfrow = c(7,1), mar = c(0.1,2,0.5,0.1) + 0.1)

#hist(b73_p$log_B73_FPKM, main = "", xaxt = "n", xlim = c(-2,4), ylim = c(0,500), bty = "n", bty = "n", breaks = 100)
#hist(b73_n$log_B73_FPKM, main = "", xaxt = "n", xlim = c(-2,4), ylim = c(0,500), bty = "n", bty = "n", breaks = 100)
#hist(td_p$log_Tdactyloides_FPKM, main = "", xaxt = "n", xlim = c(-2,4), ylim = c(0,500), bty = "n", bty = "n", breaks = 100)
#hist(td_n$log_Tdactyloides_FPKM, main = "", xaxt = "n", xlim = c(-2,4), ylim = c(0,500), bty = "n", bty = "n", breaks = 100)
#hist(tf_p$log_Tfloridanum_FPKM, main = "", xaxt = "n", xlim = c(-2,4), ylim = c(0,500), bty = "n", bty = "n", breaks = 100)
#hist(tf_n$log_Tfloridanum_FPKM, main = "", xaxt = "n", xlim = c(-2,4), ylim = c(0,500), bty = "n", bty = "n", breaks = 100)
